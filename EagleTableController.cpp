#include "EagleTableController.h"

const std::vector<std::string> EagleTableController::BaseSectionsGroupByName{ "Part", "Value", "Package" };
const std::vector<std::string> EagleTableController::BaseSectionsGroupByValue{ "Qty", "Value", "Parts", "Package" };

EagleTableController::EagleTableController(const QString &schematic, QObject *parent)
    : QAbstractTableModel(parent)
    , m_columns({ "Part", "Value", "Package" })
    , m_model(std::make_unique<EagleTableModel>(schematic.toStdString()))
    , m_grouping(false) {
    insertColumns({ "Layer" }, static_cast<int>(m_columns.size()));
    insertColumns(m_model->attributes(), static_cast<int>(m_columns.size()));
}

void EagleTableController::setSchematic(const QString &schematic) {
    std::unique_ptr<EagleTableModel> model = std::make_unique<EagleTableModel>(schematic.toStdString());
    m_commands.flush();

    int oldRowCount = m_model->numParts(m_grouping);
    int newRowCount = model->numParts(m_grouping);
    beginResetModel();
    if (newRowCount > oldRowCount) {
        beginInsertRows(QModelIndex(), oldRowCount, newRowCount - 1);
        m_model = std::move(model);
        endInsertRows();
    } else if (newRowCount < oldRowCount) {
        beginRemoveRows(QModelIndex(), newRowCount, oldRowCount - 1);
        m_model = std::move(model);
        endRemoveRows();
    }else{
        m_model = std::move(model);
    }
    endResetModel();

    // +1 due to the layer column
    const int attrStartIndex = m_grouping ? static_cast<int>(BaseSectionsGroupByValue.size()) + 1 :
                                            static_cast<int>(BaseSectionsGroupByName.size()) + 1;
    removeColumns(attrStartIndex, static_cast<int>(m_columns.size()) - attrStartIndex);
    insertColumns(m_model->attributes(), static_cast<int>(m_columns.size()));
}

void EagleTableController::insertColumn(const std::string &section, int index) {
    if (!hasColumn(section)) {
        if (index > static_cast<int>(m_columns.size()))
            index = static_cast<int>(m_columns.size());
        else if (index < 0)
            index = 0;
        beginInsertColumns(QModelIndex(), index, index);
        auto it = m_columns.begin();
        std::advance(it, index);
        m_columns.insert(it, section);
        endInsertColumns();
    }
}
void EagleTableController::insertColumns(const std::vector<std::string> columns, int index) {
    if (index >= 0 && index <= m_columns.size()) {
        const int first = index;
        const int last  = index + static_cast<int>(columns.size()) - 1;
        beginInsertColumns(QModelIndex(), first, last);
        auto beginIt = m_columns.begin();
        std::advance(beginIt, index);
        m_columns.insert(beginIt, columns.begin(), columns.end());
        endInsertColumns();
    }
}
const std::list<std::string> &EagleTableController::columns() const { return m_columns; }
bool EagleTableController::hasColumn(const std::string &section) const { return indexOfColumn(section) >= 0; }
int  EagleTableController::indexOfColumn(const std::string &section) const {
    int idx = 0;
    for (const std::string &s : m_columns) {
        if (s == section)
            return idx;
        else
            idx++;
    }
    return -1;
}
int EagleTableController::indexOfRow(const eagle::Part *part) const {
    int idx = 0;
    if (m_grouping) {
        for (const eagle::Part *p : m_model->groupedPartsKeys()) {
            if (p == part) return idx;
            idx++;
        }
    } else {
        for (const eagle::Part *p : m_model->parts()) {
            if (p == part) return idx;
            idx++;
        }
    }
    return -1;
}
const std::string EagleTableController::columnAt(int index) const {
    assert(index >= 0 && index < m_columns.size());
    auto it = m_columns.begin();
    std::advance(it, index);
    return *it;
}
void EagleTableController::removeColumn(const std::string &section) {
    int colIndex = indexOfColumn(section);
    assert(colIndex >= 0);
    beginRemoveColumns(QModelIndex(), colIndex, colIndex);
    auto it = m_columns.begin();
    std::advance(it, colIndex);
    m_columns.erase(it);
    endRemoveColumns();
}
void EagleTableController::removeColumns(const std::vector<std::string> columns) {
    for (const std::string &section : columns) { removeColumn(section); }
}
bool EagleTableController::removeColumn(int column, const QModelIndex &parent) {
    assert(column < m_columns.size());
    beginRemoveColumns(QModelIndex(), column, column);
    auto it = m_columns.begin();
    std::advance(it, column);
    m_columns.erase(it);
    endRemoveColumns();
    return true;
}
bool EagleTableController::removeColumns(int column, int count, const QModelIndex &parent) {
    int first = column;
    int last  = column + count - 1;
    assert(first <= last && first >= 0 && last < m_columns.size());
    beginRemoveColumns(parent, first, last);
    auto beginIt = m_columns.begin();
    std::advance(beginIt, first);
    auto endIt = m_columns.begin();
    std::advance(endIt, last + 1);
    m_columns.erase(beginIt, endIt);
    endRemoveColumns();
    return true;
}

int EagleTableController::rowCount(const QModelIndex &parent) const { return m_model->numParts(m_grouping); }
int EagleTableController::columnCount(const QModelIndex &parent) const { return static_cast<int>(m_columns.size()); }
QVariant EagleTableController::headerData(int section, Qt::Orientation orientation, int role) const {
    if (role != Qt::DisplayRole)
        return QVariant();
    else if (orientation == Qt::Orientation::Horizontal) {
        const std::string &columnName = columnAt(section);
        return QString::fromStdString(columnName);
    } else if (orientation == Qt::Orientation::Vertical) {
        return section + 1;  // row numbers
    } else {
        return QVariant();
    }
}
QVariant EagleTableController::data(const QModelIndex &index, int role) const {
    const int row = index.row();
    const int col = index.column();
    if (const eagle::Part *part = m_model->partAt(row, m_grouping)) {
        const std::string &columnName = columnAt(col);

        if (role == Qt::ItemDataRole::TextAlignmentRole) {
            if (columnName.rfind("Part", 0) == 0)  // columnName.startsWith("Part")
                return int(Qt::AlignmentFlag::AlignLeft | Qt::AlignmentFlag::AlignVCenter);
            else if (columnName == "Value") {
                if (part->isValueType()) return int(Qt::AlignmentFlag::AlignRight | Qt::AlignmentFlag::AlignVCenter);
            }
        } else if (role == Qt::ItemDataRole::DisplayRole || role == Qt::ItemDataRole::EditRole) {
            if (columnName == "Qty") {
                if (m_grouping) return m_model->qtyForParts(part);
            } else if (columnName == "Part") {
                return QString::fromStdString(part->name());
            } else if (columnName == "Parts") {
                if (m_grouping) {
                    const std::vector<const eagle::Part *>          &parts           = m_model->partsForKey(part);
                    std::vector<const eagle::Part *>::const_iterator end_predecessor = std::prev(parts.end());

                    std::string partNames;
                    for (std::vector<const eagle::Part *>::const_iterator it = parts.begin(); it != end_predecessor;
                         it++) {
                        partNames += (*it)->name() + ", ";
                    }
                    partNames += (*end_predecessor)->name();
                    return QString::fromStdString(partNames);
                }
            } else if (columnName == "Value") {
                return QString::fromStdString(part->value());
            } else if (columnName == "Package") {
                return QString::fromStdString(part->device()->name());
            } else if (columnName == "Layer") {
                auto locations = m_model->boardLayersForPart(part, m_grouping);
                if ((locations.first + locations.second) == 1) {
                    if (locations.first)
                        return QString("Top");
                    else
                        return QString("Bottom");
                } else {
                    if (locations.first) {
                        if (locations.second)
                            return QString("Top(%1), Bottom(%2)").arg(locations.first).arg(locations.second);
                        else
                            return QString("Top");

                    } else {
                        if (locations.second) return QString("Bottom");
                    }
                }
            } else {
                if (part->hasAttribute(columnName)) {
                    return QString::fromStdString(part->attribute(columnName));
                } else {
                    return QVariant();
                }
            }
        }
    }
    return QVariant();
}

bool EagleTableController::setData(const QModelIndex &index, const QVariant &value, int role) {
    if (role == Qt::ItemDataRole::EditRole) {
        const int          row = index.row(), col = index.column();
        const std::string  newValue = value.toString().toStdString();
        const eagle::Part *part     = partAt(row);

        if (m_grouping) {
            auto parts = m_model->partsForKey(part);
            if (col == 1) {  // Value
                m_commands.push(new eagle::ChangeValueCommand(parts, newValue));
            } else {
                m_commands.push(new eagle::AddAttributeCommand(parts, columnAt(col), newValue));
            }
        } else {
            if (col == 1) {  // Value
                m_commands.push(new eagle::ChangeValueCommand({ part }, newValue));
            } else {
                m_commands.push(new eagle::AddAttributeCommand({ part }, columnAt(col), newValue));
            }
        }
        emit dataChanged(index, index, QVector<int>(col, role));

        auto removedKeys = m_model->partsChanged({ part }, m_grouping);
        if (!removedKeys.empty() && m_grouping) { emit forceUpdate(); }
        return true;
    }
    return false;
}

Qt::ItemFlags EagleTableController::flags(const QModelIndex &index) const {
    if (index.isValid()) {
        const int                      col     = index.column();
        const std::string              colName = columnAt(col);
        const std::vector<std::string> nonEditableColumns{ "Qty", "Part", "Package", "Layer" };
        // if column.startsWith any of "nonEditableColumns"
        if (std::find_if(nonEditableColumns.begin(), nonEditableColumns.end(),
                         [&](const std::string s) { return (colName.rfind(s) == 0); }) == nonEditableColumns.end()) {
            const eagle::Part *part          = partAt(index.row());
            bool               isValueColumn = (colName == "Value");
            // All attributes are editable and parts with enabled user value
            if ((isValueColumn && part->device()->deviceset()->hasUserValue()) || !isValueColumn) {
                return QAbstractTableModel::flags(index) | Qt::ItemFlag::ItemIsEditable;
            }
        }
    }
    return QAbstractTableModel::flags(index);
}

void EagleTableController::setGrouping(bool enable) {
    if (enable != m_grouping) {
        int oldRowCount = m_model->numParts(m_grouping);
        int newRowCount = m_model->numParts(enable);
        if (newRowCount > oldRowCount) {
            beginInsertRows(QModelIndex(), oldRowCount, newRowCount - 1);
            endInsertRows();
        } else if (newRowCount < oldRowCount) {
            beginRemoveRows(QModelIndex(), newRowCount, oldRowCount - 1);
            endRemoveRows();
        }

        if (enable) {
            removeColumns(0, static_cast<int>(BaseSectionsGroupByName.size()));
            insertColumns(BaseSectionsGroupByValue, 0);
        } else {
            removeColumns(0, static_cast<int>(BaseSectionsGroupByValue.size()));
            insertColumns(BaseSectionsGroupByName, 0);
        }

        m_grouping = enable;
    }
}

int EagleTableController::numBaseColumns() const {
    if (m_grouping) { return static_cast<int>(BaseSectionsGroupByValue.size()); }
    return static_cast<int>(BaseSectionsGroupByName.size());
}

int EagleTableController::numParts(bool grouped) const { return m_model->numParts(grouped); }

const eagle::Part *EagleTableController::partAt(int index) const { return m_model->partAt(index, m_grouping); }

const std::vector<const eagle::Part *> &EagleTableController::parts() const { return m_model->parts(); }
const std::vector<const eagle::Part *> &EagleTableController::originalParts() const { return m_model->originalParts(); }
const eagle::Sheet                     *EagleTableController::sheetForPart(const eagle::Part *part) const {
    return m_model->sheetForPart(part);
}

void EagleTableController::undo() {
    if (auto cmd = m_commands.undo()) { 
        m_model->partsChanged(cmd->parts(), false); 
    }
}
void EagleTableController::redo() {
    if (auto cmd = m_commands.redo()) { 
        m_model->partsChanged(cmd->parts(), false); 
    }
}