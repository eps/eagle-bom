#ifndef BACKGROUNDINFOWIDGET_H
#define BACKGROUNDINFOWIDGET_H

#include <QMouseEvent>
#include <QWidget>

/**
 * @brief The BackgroundInfoWidget explains what todo when no schematic has been loaded yet
 * Like "drop schematic here" or "open"
 */
class BackgroundInfoWidget : public QWidget {
	Q_OBJECT
public:
	explicit BackgroundInfoWidget(QWidget *parent = nullptr);

	void paintEvent(QPaintEvent *event) override;

	void mouseReleaseEvent(QMouseEvent *event) override;

signals:
	void handleOpen();
};

#endif	// BACKGROUNDINFOWIDGET_H
