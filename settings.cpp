#include "settings.h"
#include <QDir>
#include <QFileInfo>
#include <QStandardPaths>
#include <fstream>
#include <iterator>
#include "nlohmann/json.hpp"

Settings::settings_data_t Settings::defaultSettings;

Settings& Settings::shared() {
    static Settings instance;
    return instance;
}

void Settings::applyDefaultSettings(bool preserveRecentFiles) {
    m_settings.setNumRecentFiles(defaultSettings.numRecentFiles());
    m_settings.settingsPath = defaultSettings.settingsPath;
    if (!preserveRecentFiles) m_settings.recentFiles.clear();
    m_settings.setViewGroupedParts(defaultSettings.viewGroupedParts());
    m_settings.setViewBoardLocations(defaultSettings.viewBoardLocations());
    m_settings.setSelectedFilterTypes({ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 });
}

Settings::Settings() {}

void Settings::load() {
    QString settingsFilename = QDir::current().absoluteFilePath("settings.json");
    QFile   settingsFile(settingsFilename);
    if (settingsFile.exists()) {  // settings file in same directory as the executable
        std::ifstream is(settingsFilename.toStdString());
        try {
            nlohmann::json j;
            is >> j;
            settings_data_t sd = j.get<settings_data_t>();
            sd.settingsPath    = settingsFilename.toStdString();
            m_settings         = sd;
        } catch (const std::exception& e) { qDebug() << e.what(); }
    } else {  // settings file app config folder
        settingsFilename =
            QStandardPaths::standardLocations(QStandardPaths::StandardLocation::AppDataLocation).first() +
            "/settings.json";
        settingsFile.setFileName(settingsFilename);
        if (settingsFile.exists()) {
            std::ifstream is(settingsFilename.toStdString());
            try {
                nlohmann::json j;
                is >> j;
                settings_data_t sd = j.get<settings_data_t>();
                sd.settingsPath    = settingsFilename.toStdString();
                m_settings         = sd;
            } catch (const std::exception& e) { qDebug() << e.what(); }

        } else {  // no settings file -> use default settings
            applyDefaultSettings(false);
        }
    }
#ifndef NDEBUG
    qDebug() << "Settings loaded from: " << settingsFilename;
#endif
}

void Settings::save() {
    QFileInfo fi(QString::fromStdString(m_settings.settingsPath));
    QDir      dir;
    if (!fi.exists()) dir.mkdir(fi.path());

    nlohmann::json j = m_settings;

    std::ofstream os(m_settings.settingsPath);
    if (os.is_open()) {
        os << j.dump(4);
#ifndef NDEBUG
        qDebug() << "Settings saved to file: " << QString::fromStdString(m_settings.settingsPath);
#endif
    } else
        qDebug() << "Settings file could not be opened";
}

unsigned int Settings::getNumRecentFiles() const { return m_settings.numRecentFiles(); }

void Settings::setNumRecentFiles(unsigned int value) { m_settings.setNumRecentFiles(value); }

void Settings::settings_data_t::setNumRecentFiles(unsigned int value) {
    m_numRecentFiles = value;

    int diff = static_cast<int>(recentFiles.size() - value);
    if (diff > 0) {  // remove files that are too many
                     //        qDebug() << "setNumRecentFiles: pop " << diff;
        for (int i = 0; i < diff; i++) { recentFiles.pop_back(); }
    }
}

std::list<std::string> Settings::getRecentFiles() const { return m_settings.recentFiles; }

void Settings::setRecentFiles(const std::list<std::string>& value) { m_settings.recentFiles = value; }

void Settings::recentFilesPush(const std::string& file) {
    m_settings.recentFiles.remove(file);  // remove it to place it at the front again
    m_settings.recentFiles.push_front(file);
    if (m_settings.recentFiles.size() > m_settings.numRecentFiles()) { m_settings.recentFiles.pop_back(); }
}

bool Settings::recentFilesRemove(const std::string& file) {
    return m_settings.recentFiles.remove(file);
}

const std::string* Settings::recentFilesPeak() const {
    if (m_settings.recentFiles.size() > 0)
        return &m_settings.recentFiles.front();
    else
        return nullptr;
}

std::string Settings::getSettingsPath() const { return m_settings.settingsPath; }

void Settings::setSettingsPath(const std::string& value) { m_settings.settingsPath = value; }

const std::list<std::string>& Settings::getVisibleAttributes() const { return m_settings.visibleAttributes; }

void Settings::setVisibleAttributes(const std::list<std::string>& attributes) {
    m_settings.visibleAttributes = attributes;
}

void to_json(nlohmann::json& j, const Settings::settings_data_t& s) {
    j = nlohmann::json{
        { "numRecentFiles", s.numRecentFiles() },     { "recentFiles", s.recentFiles },
        { "viewGroupedParts", s.viewGroupedParts() }, { "viewBoardLocations", s.viewBoardLocations() },
        { "visibleAttributes", s.visibleAttributes }, { "selectedFilterTypes", s.selectedFilterTypes() },
    };
}

void from_json(const nlohmann::json& j, Settings::settings_data_t& s) {
    try {
        s.setNumRecentFiles(j.at("numRecentFiles").get<unsigned int>());
    } catch (const std::exception&) { s.setNumRecentFiles(Settings::defaultSettings.numRecentFiles()); }
    try {
        s.recentFiles = j.at("recentFiles").get<std::list<std::string>>();
    } catch (const std::exception&) {}
    try {
        s.setViewGroupedParts(j.at("viewGroupedParts").get<bool>());
    } catch (const std::exception&) {}
    try {
        s.setViewBoardLocations(j.at("viewBoardLocations").get<bool>());
    } catch (const std::exception&) {}
    try {
        s.visibleAttributes = j.at("visibleAttributes").get<std::list<std::string>>();
    } catch (const std::exception&) {}
    try {
        s.setSelectedFilterTypes(j.at("selectedFilterTypes").get<std::vector<int>>());
    } catch (const std::exception&) {}
}

Settings::settings_data::settings_data(unsigned int numRecentFiles, bool viewGroupedParts, bool viewBoardLocations)
    : m_numRecentFiles(numRecentFiles)
    , m_viewGroupedParts(viewGroupedParts)
    , m_viewBoardLocations(viewBoardLocations)
    , m_selectedFilterTypes({ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 })
    , recentFiles() {
    settingsPath = (QStandardPaths::standardLocations(QStandardPaths::StandardLocation::AppDataLocation).first() +
                    "/eagle-bom/settings.json")
                       .toStdString();
}

void Settings::updateSettings(const Settings::settings_data_t& newSettings) { m_settings = newSettings; }

QDebug operator<<(QDebug debug, const std::string& str) {
    QDebugStateSaver saver(debug);
    debug.nospace() << QString::fromStdString(str);
    return debug;
}
