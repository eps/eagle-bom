#pragma once

#include <vector>

#include <QCheckBox>
#include <QLineEdit>
#include <QListView>
#include <QPushButton>
#include <QStandardItemModel>
#include <QWidget>

class AttributesView : public QWidget {
    Q_OBJECT
public:
    explicit AttributesView(QWidget *parent = nullptr);

    void setSelection(const std::vector<int> &indices);
    void select(const std::vector<int> &indices, QItemSelectionModel::SelectionFlags command);
    void select(int index, QItemSelectionModel::SelectionFlags command);
    void select(const QModelIndex &index, QItemSelectionModel::SelectionFlags command) {
        m_listView->selectionModel()->select(index, command);
    }
    void        selectedAttributes(std::list<std::string> &outAttributes);
    void        setAttributes(const std::vector<std::string> &attributes);
    int         attributesCount() { return m_model->rowCount(); }
    bool        attributeExists(const QString &attribute);
    std::string attributeForIndex(int i) {
        const QModelIndex &index = m_model->index(i, 0);
        return m_model->itemFromIndex(index)->text().toStdString();
    }

signals:
    void selectionChanged(const QItemSelection &selected, const QItemSelection &deselected);
    void attributeAdded(QString attribute, int index);
    void attributeDeleted(int index);

protected slots:
    void selectAll();
    void clearSelection();
    void lineEditReturnPressed();

private:
    QListView *         m_listView;
    QStandardItemModel *m_model;
    QLineEdit *         m_addAttributeLineEdit;

    int  addAttribute(const QString &attr);
    int  addAttribute(const std::string &attr);
    void deleteAttribute(int index);

    bool eventFilter(QObject *obj, QEvent *event) override;
};
