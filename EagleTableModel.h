#ifndef EAGLETABLEMODEL_H
#define EAGLETABLEMODEL_H

#include <map>
#include <memory>
#include <vector>

#include "EagleLib/commands/CommandHistory.h"
#include "EagleLib/part.h"
#include "EagleLib/schematic.h"

class EagleTableModel {
public:
    explicit EagleTableModel(const std::string& schematicPath);
    ~EagleTableModel();

    void                                   setSchematic(const std::string& schematicPath);
    int                                    numParts(bool grouped) const;
    const eagle::Part*                     partAt(int index, bool grouped) const;
    const std::vector<const eagle::Part*>& partsForKey(const eagle::Part* part);
    int                                    qtyForParts(const eagle::Part* key) const;
    std::pair<int, int>                    boardLayersForPart(const eagle::Part* part, bool grouped) const;
    const std::vector<std::string>&        attributes() const;
    const std::vector<const eagle::Part*>& parts() const;
    const std::vector<const eagle::Part*>& originalParts() const;
    const std::map<const eagle::Part*, std::vector<const eagle::Part*>>& groupedParts() const;
    const std::vector<const eagle::Part*>&                               groupedPartsKeys() const;
    const eagle::Sheet*                                                  sheetForPart(const eagle::Part* part) const;

    /**
     * @brief Has to be called whenever parts were changed so that
     * groupedParts stays consistent
     *
     * @param parts Parts that have been changed
     * @param partsAreKeys If true then parts are interpreted as keys
     * @return vector of keys that were removed
     */
    std::vector<const eagle::Part*> partsChanged(const std::vector<const eagle::Part*>& parts, bool partsAreKeys);

private:
    std::unique_ptr<eagle::Schematic>                             m_schematic;
    eagle::CommandHistory                                         m_commands;
    std::vector<std::string>                                      m_attributes;
    std::vector<const eagle::Part*>                               m_originalParts;
    std::vector<const eagle::Part*>                               m_groupedPartsKeys;
    std::map<const eagle::Part*, std::vector<const eagle::Part*>> m_groupedParts;

    const eagle::Part* keyForPart(const eagle::Part* part) const;
    void               mergeKeys(const std::vector<const eagle::Part*> keys);
};

#endif  // EAGLETABLEMODEL_H
