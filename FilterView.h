#pragma once

#include <vector>

#include <QLineEdit>
#include <QListView>
#include <QPushButton>
#include <QCheckBox>
#include <QStandardItemModel>
#include <QWidget>


class FilterView : public QWidget {
    Q_OBJECT
public:
    explicit FilterView(QWidget *parent = nullptr);

    void setSelection(const std::vector<int> &indices);
    void setBoardLayers(bool checked);
    bool showBoardLayers() const;
    void select(const std::vector<int> &indices, QItemSelectionModel::SelectionFlags command);
    void selectedFilterTypes(std::vector<int>& outVector) const;
    QString searchText();

signals:
    void selectionChanged(const QItemSelection &selected, const QItemSelection &deselected);
    void showBoardLayersToggled(bool checked);
    void searchTextChanged(QString newText);

protected slots:
    void selectAll();
    void clearSelection();

private:
    QListView          *m_listView;
    QStandardItemModel *m_model;
    QPushButton        *m_selectAllButton;
    QPushButton        *m_selectNoneButton;
    QCheckBox          *m_boardLayersCheckbox;
    QLineEdit          *m_searchLineEdit;
};
