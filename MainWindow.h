#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <list>
#include <regex>
#include <vector>

#include <QAction>
#include <QCloseEvent>
#include <QDropEvent>
#include <QMainWindow>
#include <QMenu>
#include <QMouseEvent>
#include <QStandardItemModel>

#include "EagleLib/commands/CommandHistory.h"
#include "EagleLib/schematic.h"

#include "AttributesView.h"
#include "BackgroundInfoWidget.h"
#include "EagleTableController.h"
#include "EagleTableControllerProxy.h"
#include "FilterView.h"
#include "OverlayViewController.h"
#include "TableView.h"

class MainWindow : public QMainWindow {
    Q_OBJECT
public:
    MainWindow(QWidget *parent = nullptr);

    //    void tableViewActivated(const QModelIndex &proxyIndex);
    void dragEnterEvent(QDragEnterEvent *event);
    void dropEvent(QDropEvent *event);
    //	void mouseReleaseEvent(QMouseEvent *event);
    //    bool eventFilter(QObject *object, QEvent *event);

public slots:
    void tableViewShowContextMenu(const QPoint &pos);

    void fileMenuOpenAction(bool checked);
    void fileMenuAboutToShow();
    void fileMenuRevealInExplorerAction(bool checked);
    void fileMenuRecentFilesAboutToShow();
    void fileMenuReloadAction(bool checked);
    void fileExportMenuBOMAction(bool checked);
    void fileExportMenuSMDMountingAction(bool checked);
    void fileExportMenuEagleChangeScriptAction(bool checked);
    void fileMenuQuitAction(bool checked);
    void editMenuUndoAction(bool checked);
    void editMenuRedoAction(bool checked);
    void viewMenuGroupedAction(bool checked);
    void toolMenuAboutAction(bool checked);
    // void utilityMenuExportSDCAction(bool checked);
    // void utilityMenuTestAction(bool checked);

    void closeEvent(QCloseEvent *event) { fileMenuQuitAction(true); }

    void backgroundInfoViewHandleOpen();

    void attributesViewSelectionChanged(const QItemSelection &selected, const QItemSelection &deselected);
    void filterViewSelectionChanged(const QItemSelection &selected, const QItemSelection &deselected);
    void filterViewShowBoardLayersToggled(bool checked);
    void filterViewSearchTextChanged(QString newText);

private:
    TableView                 *m_tableView;
    EagleTableController      *m_tableController;
    EagleTableControllerProxy *m_tableControllerProxy;

    OverlayViewController *m_overlayVC;
    BackgroundInfoWidget  *m_backgroundInfoView;
    AttributesView        *m_attributesView;
    // QStandardItemModel    *m_attributesModel;
    FilterView            *m_filterView;

    QMenu   *m_fileMenu;
    QAction *m_fileMenuOpenAction;
    QAction *m_fileMenuRevealInExplorerAction;
    QMenu   *m_fileMenuRecentFilesMenu;
    QAction *m_fileMenuReloadAction;
    QMenu   *m_fileExportMenu;
    QAction *m_fileExportMenuBOMAction;
    QAction *m_fileExportMenuSMDMountingAction;
    QAction *m_fileExportMenuEagleChangeScriptAction;
    QAction *m_fileMenuQuitAction;

    QMenu   *m_editMenu;
    QAction *m_editMenuUndoAction;
    QAction *m_editMenuRedoAction;

    QMenu   *m_viewMenu;
    QAction *m_viewMenuGroupedAction;

    QMenu   *m_toolMenu;
    QAction *m_toolMenuAboutAction;

    // QMenu   *m_utilityMenu;
    // QAction *m_utilityMenuExportSDC;

    void createMenu();

    /**
     * @brief (Re-)Loads a schematic
     *
     * @param schematic_path Path to Schematic
     * @param reload_settings True: Settings are read from disk (usually loading another schematic), False: UI stays as
     * it is (usually reloading the same schematic)
     */
    void loadSchematic(const QString &schematic_path, bool reload_settings);
    // const QVector<unsigned int> &defaultSortOrder() const;

    // void attributesViewSelectedAttributes(std::list<std::string> &outList) const;
    void attributesViewAttributeAdded(QString attr, int index);
    void attributesViewAttributeDeleted(int index);
};
#endif  // MAINWINDOW_H
