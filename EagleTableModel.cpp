#include "EagleTableModel.h"

EagleTableModel::EagleTableModel(const std::string& schematicPath) { setSchematic(schematicPath); }

EagleTableModel::~EagleTableModel() {
    for (auto p : m_originalParts) { delete p; }
}

void EagleTableModel::setSchematic(const std::string& schematicPath) {
    m_schematic = std::make_unique<eagle::Schematic>(schematicPath);
    std::vector<std::string> newAttributes;
    m_schematic->uniqueAttributes(m_attributes, true);
    m_schematic->groupPartsByValue(m_schematic->parts(), m_groupedParts, m_groupedPartsKeys, true);

    // create a deep copy of the parts
    for (const eagle::Part* p : m_schematic->parts()) {
        eagle::Part* duplicatedPart = new eagle::Part(*p);
        m_originalParts.push_back(duplicatedPart);
    }
}

int EagleTableModel::numParts(bool grouped) const {
    if (grouped) {
        return static_cast<int>(m_groupedPartsKeys.size());
    } else {
        return static_cast<int>(m_schematic->parts().size());
    }
}

const eagle::Part* EagleTableModel::partAt(int index, bool grouped) const {
    if (grouped) {
        if (index >= 0 && index < m_groupedPartsKeys.size()) { return m_groupedPartsKeys[index]; }
    } else {
        auto parts = m_schematic->parts();
        if (index >= 0 && index < parts.size()) { return m_schematic->parts()[index]; }
    }
    return nullptr;
}

const std::vector<const eagle::Part*>& EagleTableModel::partsForKey(const eagle::Part* part) {
    return m_groupedParts.at(part);
}

int EagleTableModel::qtyForParts(const eagle::Part* key) const {
    return static_cast<int>(m_groupedParts.at(key).size());
}

std::pair<int, int> EagleTableModel::boardLayersForPart(const eagle::Part* part, bool grouped) const {
    int top = 0, bottom = 0;
    if (grouped) {
        for (auto part : m_groupedParts.at(part)) {
            if (part->boardLocation().bottom())
                bottom++;
            else
                top++;
        }
    } else {
        auto it = m_schematic->parts().begin();
        if (std::find(it, m_schematic->parts().end(), part) != m_schematic->parts().end()) {
            if ((*it)->boardLocation().bottom())
                bottom = 1;
            else
                top = 1;
        }
    }
    return { top, bottom };
}

const std::vector<std::string>& EagleTableModel::attributes() const { return m_attributes; }

const std::vector<const eagle::Part*>& EagleTableModel::parts() const { return m_schematic->parts(); }
const std::vector<const eagle::Part*>& EagleTableModel::originalParts() const { return m_originalParts; }
const eagle::Sheet*                    EagleTableModel::sheetForPart(const eagle::Part* part) const {
                       return m_schematic->sheetForPart(part);
}

const std::map<const eagle::Part*, std::vector<const eagle::Part*>>& EagleTableModel::groupedParts() const {
    return m_groupedParts;
}

const std::vector<const eagle::Part*>& EagleTableModel::groupedPartsKeys() const { return m_groupedPartsKeys; }

const eagle::Part* EagleTableModel::keyForPart(const eagle::Part* part) const {
    for (auto [k, v] : m_groupedParts) {
        if (std::find(v.begin(), v.end(), part) != v.end()) return k;
    }
    return nullptr;
}

void EagleTableModel::mergeKeys(const std::vector<const eagle::Part*> keys) {
    std::vector<const eagle::Part*>& v0 = m_groupedParts.at(keys.front());
    auto                             it = keys.begin() + 1;
    for (; it != keys.end(); it++) {
        auto vn = m_groupedParts.at(*it);
        v0.insert(v0.end(), std::make_move_iterator(vn.begin()),
                  std::make_move_iterator(vn.end()));  // move elements from vn to v0
        m_groupedParts.erase(*it);                     // remove key-value pair

        // find key also in groupedKeys vector
        auto keysVectorIt = std::find(m_groupedPartsKeys.begin(), m_groupedPartsKeys.end(), *it);
        if (keysVectorIt != m_groupedPartsKeys.end()) { m_groupedPartsKeys.erase(keysVectorIt); }
    }
}

std::vector<const eagle::Part*> EagleTableModel::partsChanged(const std::vector<const eagle::Part*>& parts,
                                                              bool                                   partsAreKeys) {
    std::vector<const eagle::Part*> removedKeys;
    for (auto part : parts) {
        if (partsAreKeys) {
            // cases to consider
            // - keys may now be duplicates -> merge

            // check if key does match any existing key
            std::vector<const eagle::Part*> equalKeys;
            for (const eagle::Part* key : m_groupedPartsKeys) {
                if (*key == *part) { equalKeys.push_back(key); }
            }

            if (equalKeys.size() > 1) {
                mergeKeys(equalKeys);
                removedKeys.insert(removedKeys.end(), equalKeys.begin() + 1, equalKeys.end());
            }
        } else {
            if (auto previousKey = keyForPart(part)) {
                // cases to consider
                //  - when part is also the key and vector is not empty after removal
                //  - when part is also the key and part is only element in vector
                //  - keys have to be updated in groupedPartsKeys as well

                std::vector<const eagle::Part*>& values = m_groupedParts.at(previousKey);

                if (values.size() > 1) {        // vector will not be empty after removal
                    if (part == previousKey) {  // part is also the key
                        // move other parts in new map-bucket
                        // if (part == previousKey) then part is always first element in vector
                        std::vector<const eagle::Part*> newValues;
                        newValues.insert(newValues.end(), std::make_move_iterator(values.begin() + 1),
                                         std::make_move_iterator(values.end()));
                        values.erase(values.begin() + 1, values.end());

                        // check if the parts in the new vector match any existing key
                        std::vector<const eagle::Part*> equalKeys;
                        for (const eagle::Part* key : m_groupedPartsKeys) {
                            if (*key == *newValues.front()) { equalKeys.push_back(key); }
                        }

                        if (equalKeys.empty()) {  // newValues will be in a new key
                            m_groupedParts[newValues.front()] = newValues;
                            m_groupedPartsKeys.push_back(newValues.front());
                        } else {
                            // newValues will be inserted into equalKeys.front().values
                            std::vector<const eagle::Part*>& equalKeysFrontValues =
                                m_groupedParts.at(equalKeys.front());
                            std::copy(newValues.begin(), newValues.end(), std::back_inserter((equalKeysFrontValues)));
                            // if equalKeys.size() > 1 -> merge remaining equalKeys with first equalKey
                            if (equalKeys.size() > 1) {
                                mergeKeys(equalKeys);
                                removedKeys.insert(removedKeys.end(), equalKeys.begin() + 1, equalKeys.end());
                            }
                        }
                    } else {
                        if (*part != *previousKey) {  // only perform changes if the part does not fit into current key
                            // remove part from old vector
                            auto it = std::find(values.begin(), values.end(), part);
                            if (it != values.end()) { values.erase(it); }

                            // check if the part matches any existing key
                            std::vector<const eagle::Part*> equalKeys;
                            for (const eagle::Part* key : m_groupedPartsKeys) {
                                if (*key == *part) { equalKeys.push_back(key); }
                            }

                            // place part in new map-bucket
                            if (!equalKeys.empty()) {  // newKey exists
                                // insert part in new vector
                                m_groupedParts.at(equalKeys.front()).push_back(part);
                                // if equalKeys.size() > 1 -> merge remaining equalKeys with first equalKey

                                if (equalKeys.size() > 1) {
                                    mergeKeys(equalKeys);
                                    removedKeys.insert(removedKeys.end(), equalKeys.begin() + 1, equalKeys.end());
                                }

                            } else {  // newKey does not exist
                                      // key has to be created
                                m_groupedParts[part] = { part };
                                m_groupedPartsKeys.push_back(part);
                            }
                        }
                    }
                } else {  // else (values.size == 1) no change on map entry since key and part are equal and a change
                          // does not affect this container
                    // check if the parts in the new vector match any existing key
                    std::vector<const eagle::Part*> equalKeys;
                    for (const eagle::Part* key : m_groupedPartsKeys) {
                        if (*key == *part) { equalKeys.push_back(key); }
                    }
                    if (equalKeys.size() > 1) {
                        mergeKeys(equalKeys);
                        removedKeys.insert(removedKeys.end(), equalKeys.begin() + 1, equalKeys.end());
                    }
                }
            } else {
                assert(true);  // reaching here means that the part is not sorted into groupedParts -> should never be
                               // the case
            }
        }
    }
    return removedKeys;
}