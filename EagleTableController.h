#pragma once

#include <list>
#include <vector>

#include <QAbstractTableModel>
#include <QDebug>

// #include <QRegExp>
// #include <QRegularExpression>
#include "EagleLib/commands/CommandHistory.h"
#include "EagleLib/part.h"
#include "EagleTableModel.h"

class EagleTableController : public QAbstractTableModel {
    Q_OBJECT
public:
    explicit EagleTableController(const QString &schematic, QObject *parent = nullptr);

    void                                    setSchematic(const QString &schematic);
    void                                    setGrouping(bool enable);
    bool                                    grouping() const { return m_grouping; }
    int                                     numBaseColumns() const;
    int                                     numParts(bool grouped) const;
    const eagle::Part                      *partAt(int index) const;
    const std::vector<const eagle::Part *> &parts() const;
    const std::vector<const eagle::Part *> &originalParts() const;
    const eagle::Sheet* sheetForPart(const eagle::Part* part) const;

    void undo();
    void redo();

    void                          insertColumn(const std::string &section, int index);
    void                          insertColumns(const std::vector<std::string> columns, int index);
    bool                          hasColumn(const std::string &section) const;
    int                           indexOfColumn(const std::string &section) const;
    int                           indexOfRow(const eagle::Part *part) const;
    const std::string             columnAt(int index) const;
    const std::list<std::string> &columns() const;
    void                          removeColumn(const std::string &section);
    void                          removeColumns(const std::vector<std::string> columns);
    bool                          removeColumn(int column, const QModelIndex &parent = QModelIndex());
    bool                          removeColumns(int column, int count, const QModelIndex &parent = QModelIndex());

    const std::vector<std::string> &attributes() const { return m_model->attributes(); }

    // QAbstractItemModel interface
    int           rowCount(const QModelIndex &parent = QModelIndex()) const;
    int           columnCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant      headerData(int section, Qt::Orientation orientation, int role) const;
    QVariant      data(const QModelIndex &index, int role) const;
    bool          setData(const QModelIndex &index, const QVariant &value, int role);
    Qt::ItemFlags flags(const QModelIndex &index) const;

signals:
    void forceUpdate();

private:
    static const std::vector<std::string> BaseSectionsGroupByName;
    static const std::vector<std::string> BaseSectionsGroupByValue;

    std::list<std::string>           m_columns;
    std::unique_ptr<EagleTableModel> m_model;
    bool                             m_grouping;
    eagle::CommandHistory            m_commands;
};
