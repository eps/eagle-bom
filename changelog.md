# Changelog

20230621:
- The change script raised errors if the value consists spaces. Therefore, the value in the change script is quoted now.
- If the filename had more than one dot in it the file name for the exported files were truncated after the first dot. Now the extension is replaced correctly.