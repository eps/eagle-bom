#include "BackgroundInfoWidget.h"

#include <QDebug>
#include <QPainter>

BackgroundInfoWidget::BackgroundInfoWidget(QWidget *parent) : QWidget(parent) { setMinimumSize({ 340, 240 }); }

void BackgroundInfoWidget::paintEvent(QPaintEvent * /*event*/) {
    QPainter painter(this);

    const QPointF center(width() / 2.0, height() / 2.0);
    const QSizeF  size(340, 240);
    const QRectF  rect(center.x() - size.width() / 2, center.y() - size.height() / 2, size.width(), size.height());

    QPen p(painter.pen());
    p.setWidth(3);
    p.setStyle(Qt::PenStyle::DashLine);
    p.setColor(Qt::GlobalColor::lightGray);

    QFont font = painter.font();
    font.setPointSize(24);
    painter.setFont(font);

    painter.setPen(p);
    painter.drawRoundedRect(rect, 40, 40);
    painter.drawText(rect, Qt::AlignCenter, "Drop an\nEAGLE Schematic here\nor\nOpen one via CTRL+O");
}

void BackgroundInfoWidget::mouseReleaseEvent(QMouseEvent *event) {
    if (event->button() == Qt::MouseButton::LeftButton) {
        emit handleOpen();
    } else
        QWidget::mouseReleaseEvent(event);
}
