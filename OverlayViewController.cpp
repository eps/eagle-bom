#include "OverlayViewController.h"
#include "ShadowEffect.h"
#include <QDebug>
#include <QGridLayout>
#include <QParallelAnimationGroup>
#include <QPropertyAnimation>
#include <QStackedLayout>

QWidget *OverlayViewController::baseWidget() const {
    if(QStackedLayout *l = qobject_cast<QStackedLayout *>(this->layout())) { return l->widget(0); }
    return nullptr;
}

void OverlayViewController::setBaseWidget(QWidget *widget) {
    if(QStackedLayout *l = qobject_cast<QStackedLayout *>(this->layout())) {
        if(QWidget *wdg = l->widget(0)) {    // remove existing base widget
            l->removeWidget(wdg);
            wdg->deleteLater();
        }
        l->insertWidget(0, widget);
        widget->setParent(this);
	}
}

QWidget *OverlayViewController::overlayWidget() const {
    if(QStackedLayout *l = qobject_cast<QStackedLayout *>(this->layout())) {
        if(l->count() > 0) {
            return l->widget(1)->findChild<QWidget *>("", Qt::FindChildOption::FindDirectChildrenOnly);
        }
    }
    return nullptr;
}

void OverlayViewController::setOverlayWidget(QWidget *overlayWidget) {
    if(QStackedLayout *l = qobject_cast<QStackedLayout *>(this->layout())) {
        QSize overlay_size = overlayWidget->size() + QSize(2 * m_shadowBlurRadius, 2 * m_shadowBlurRadius);

        QWidget *parentWidget = this->parentWidget();
        QRect    win_size(parentWidget->pos(), parentWidget->size());

        if(overlay_size.width() > win_size.width() || overlay_size.height() > win_size.height()) {
            // window has to be increase to fit the overlay
            int h_adjust = (overlay_size.width() - win_size.width()) / 2;
            int v_adjust = (overlay_size.height() - win_size.height()) / 2;
            if(h_adjust < 0) h_adjust = 0;
            if(v_adjust < 0) v_adjust = 0;

            QRect newRect = win_size.adjusted(-h_adjust, -v_adjust, h_adjust, v_adjust);

            // increase size if widget is too small
            QParallelAnimationGroup *group = new QParallelAnimationGroup;

            QPropertyAnimation *originAnimation = new QPropertyAnimation(parentWidget, "pos");
            originAnimation->setStartValue(win_size.topLeft());
            originAnimation->setEndValue(newRect.topLeft());
            originAnimation->setEasingCurve(QEasingCurve::InOutQuad);
            group->addAnimation(originAnimation);

            QPropertyAnimation *sizeAnimation = new QPropertyAnimation(parentWidget, "size");
            sizeAnimation->setStartValue(win_size.size());
            sizeAnimation->setEndValue(newRect.size());
            sizeAnimation->setEasingCurve(QEasingCurve::InOutQuad);
            group->addAnimation(sizeAnimation);

            connect(group, &QAnimationGroup::finished, [this, l, overlayWidget]() {
                this->showOverlayWidget(l, overlayWidget);
            });

            group->start();
        } else {
            if(l->count() > 1) {    // dismiss the present overlay
                QWidget *presentOverlayWdg = this->overlayWidget();
                if(typeid(*presentOverlayWdg) != typeid(*overlayWidget)) {
                    connect(this, &OverlayViewController::animationFinished, [this, l, overlayWidget]() {
                        this->showOverlayWidget(l, overlayWidget);
                        disconnect(this, &OverlayViewController::animationFinished, this, nullptr);
                    });
                    this->dismissOverlay();
                }
            } else {
                showOverlayWidget(l, overlayWidget);
            }
        }
    } else {
        qDebug() << "OverlayViewController::setOverlayWidget does not have a QStackedLayout!";
    }
}

bool OverlayViewController::hasOverlayWidget() const {
    if(this->overlayWidget()) return true;
    else
        return false;
}

bool OverlayViewController::hasBaseWidget() const {
    if(this->baseWidget()) return true;
    else
        return false;
}

void OverlayViewController::dismissOverlay() {
    if(QStackedLayout *l = qobject_cast<QStackedLayout *>(this->layout())) {
        if(l->count() > 0) {    // check if there is an overlay
            QWidget *wdg = l->widget(1);
            if(QWidget *overlayWidget = this->overlayWidget()) {
                // remove overlaywidget from layout so we can handle its position
                wdg->layout()->removeWidget(overlayWidget);

                QPoint start(overlayWidget->pos());
                QPoint end(start);
                end.setY(end.y() + overlayWidget->height());

                QPropertyAnimation *anim = new QPropertyAnimation(overlayWidget, "pos");
                anim->setEasingCurve(QEasingCurve::OutCubic);
                anim->setStartValue(start);
                anim->setEndValue(end);
                anim->start();
                connect(anim, &QPropertyAnimation::finished, [this, l, wdg, overlayWidget]() {
                    overlayWidget->deleteLater();
                    l->removeWidget(wdg);
                    wdg->deleteLater();
                    if(QWidget *baseWdg = baseWidget()) { baseWdg->setFocusProxy(nullptr); }
                    emit this->animationFinished();
                });
            }
        }
    }
}

void OverlayViewController::showOverlayWidget(QStackedLayout *l, QWidget *overlayWidget) {
    ShadowEffect *shadow = new ShadowEffect();
    shadow->setDistance(m_shadowDistance);
    shadow->setBlurRadius(m_shadowBlurRadius);
    shadow->setColor(QColor(0, 0, 0, 127));

    QWidget *wdg = new QWidget(this);
    wdg->setMinimumSize(overlayWidget->minimumSize() + QSize(2 * m_shadowBlurRadius, m_shadowBlurRadius));
    QGridLayout *gl = new QGridLayout(wdg);
    gl->setContentsMargins(0, 0, 0, 0);
    gl->setAlignment(Qt::AlignmentFlag::AlignHCenter | Qt::AlignmentFlag::AlignBottom);
    wdg->setFocusProxy(overlayWidget);

    if(QWidget *baseWdg = baseWidget()) { baseWdg->setFocusProxy(overlayWidget); }

    overlayWidget->setAutoFillBackground(true);
    overlayWidget->setGraphicsEffect(shadow);
    overlayWidget->setParent(wdg);
    overlayWidget->setFocusPolicy(Qt::FocusPolicy::StrongFocus);
    overlayWidget->setFocus();
    overlayWidget->show();
    l->addWidget(wdg);
    l->setCurrentIndex(1);

    QSize  overlaySize = overlayWidget->size();
    QPoint start(wdg->width() / 2 - overlaySize.width() / 2, wdg->pos().y() + wdg->height());
    QPoint end(start);
    end.setY(end.y() - overlaySize.height());

    QPropertyAnimation *anim = new QPropertyAnimation(overlayWidget, "pos");
    anim->setEasingCurve(QEasingCurve::OutCubic);
    anim->setStartValue(start);
    anim->setEndValue(end);
    anim->start();
    connect(anim, &QPropertyAnimation::finished, [gl, overlayWidget]() {
        gl->addWidget(overlayWidget);
    });
}

OverlayViewController::OverlayViewController(QWidget *parent)
    : QWidget(parent), m_shadowDistance(10), m_shadowBlurRadius(30) {
    QStackedLayout *layout = new QStackedLayout();
    layout->setStackingMode(QStackedLayout::StackingMode::StackAll);
    this->setLayout(layout);
    this->setBaseWidget(new QWidget());
}
