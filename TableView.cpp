#include <QClipboard>
#include <QDebug>
#include <QGuiApplication>
#include <QKeyEvent>

#include "TableView.h"
#include "EagleTableControllerProxy.h"

TableView::TableView(QWidget *parent) : QTableView(parent) {}

void TableView::keyReleaseEvent(QKeyEvent *event) {
    switch (event->key()) {
        case Qt::Key_Delete:;
        case Qt::Key_Back: {
            const auto proxyIndexes = selectedIndexes();
            if (!proxyIndexes.empty()) {
                if (QAbstractProxyModel *proxy = qobject_cast<QAbstractProxyModel *>(this->model())) {
                    if (QAbstractTableModel *model = qobject_cast<QAbstractTableModel *>(proxy->sourceModel())) {
                        for (const QModelIndex &proxyIndex : proxyIndexes) {
                            const QModelIndex sourceIndex = proxy->mapToSource(proxyIndex);
                            model->setData(sourceIndex, QString(""));
                        }
                    }
                }
                break;
            };
        }
        case Qt::Key_V: {
            if (event->modifiers() == Qt::Modifier::CTRL) {
                QString pasteString = QGuiApplication::clipboard()->text();
                if (QAbstractProxyModel *proxy = qobject_cast<QAbstractProxyModel *>(this->model())) {
                    if (QAbstractTableModel *model = qobject_cast<QAbstractTableModel *>(proxy->sourceModel())) {
                        const auto proxyIndexes = selectedIndexes();
                        if (!proxyIndexes.empty()) {
                            for (const QModelIndex &proxyIndex : proxyIndexes) {
                                const QModelIndex sourceIndex = proxy->mapToSource(proxyIndex);
                                model->setData(sourceIndex, pasteString);
                            }
                        }
                    }
                }
            }
            break;
        }
        default: break;
    }
}

// void TableView::selectionChanged(const QItemSelection &selected, const QItemSelection &deselected) {
//    emit QTableView::selectionChanged(selected, deselected);
//    // multiple selection shall only work for whole rows.
//    const auto selection = this->selectedIndexes();
//    if (!selection.empty()) {
//        if (selection.back().row() - selection.front().row() > 0) {
//            this->setSelectionBehavior(QAbstractItemView::SelectionBehavior::SelectRows);
//        } else
//            this->setSelectionBehavior(QAbstractItemView::SelectionBehavior::SelectItems);
//    }
//}
