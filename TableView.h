#ifndef TABLEVIEW_H
#define TABLEVIEW_H

#include <QTableView>

class TableView : public QTableView {
    Q_OBJECT
public:
    explicit TableView(QWidget *parent = nullptr);

    void keyReleaseEvent(QKeyEvent *event);

signals:

protected slots:
    //    void selectionChanged(const QItemSelection &selected, const QItemSelection &deselected);
};

#endif	// TABLEVIEW_H
