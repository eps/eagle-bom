# EAGLE-BOM

EAGLE-BOM is a tool meant to replace the eagle integrated bom ULP.
It implements handy features like searching, filtering, showing the part's location (top or bottom) and changing the part's attributes and values.
Changes can be written back into the schematic by exporting the changes to an eagle script file that can be executed in eagle.

![eagle-bom](docs/img/eagle-bom.png)

## How to build

You need to have the following packages installed:
- [nlohmann_json](https://github.com/nlohmann/json)
- [Qt 6.2](https://www.qt.io/)
- A compiler (tested only on windows, but should be cross compilable)

Create a `CMakeUserPresets.json` file with the following content and adjust according to your installations of Visual Studio and Qt

```json
{
    "version": 4,
    "cmakeMinimumRequired": {"major": 3,"minor": 23, "patch": 0},
    "configurePresets": [
        {
            "name": "release",
            "inherits": "ninja-release",
            "cacheVariables": {
                "CMAKE_PREFIX_PATH": "C:/Users/tkruse/AppData/Local/0_gsi_executables/qt/6.2.4/msvc2022_64_static",
                "CMAKE_C_COMPILER": "C:/Program Files (x86)/Microsoft Visual Studio/2022/BuildTools/VC/Tools/MSVC/14.34.31933/bin/Hostx64/x64/cl.exe",
                "CMAKE_CXX_COMPILER" : "C:/Program Files (x86)/Microsoft Visual Studio/2022/BuildTools/VC/Tools/MSVC/14.34.31933/bin/Hostx64/x64/cl.exe"
            },
            "environment": {
                "INCLUDE": "C:/Program Files (x86)/Microsoft Visual Studio/2022/BuildTools/VC/Tools/MSVC/14.34.31933/include;C:/Program Files (x86)/Windows Kits/10/include/10.0.22000.0/ucrt",
                "LIB": "C:/Program Files (x86)/Microsoft Visual Studio/2022/BuildTools/VC/Tools/MSVC/14.34.31933/lib/x64;C:/Program Files (x86)/Windows Kits/10/lib/10.0.22000.0/um/x64;C:/Program Files (x86)/Windows Kits/10/lib/10.0.22000.0/ucrt/x64",
                "PATH": "C:/Program Files (x86)/Windows Kits/10/bin/10.0.22000.0/x64;C:/Users/tkruse/AppData/Local/0_gsi_executables"
            }
        }
    ]
}
```

```bash
# [Windows] Inside of a developers command prompt cd into this project's root folder
cmake --preset release . # configuring according to the user preset
cmake --build build/release # build
```

### Static vs dynamic build

Qt itself comes as a dynamic package.
To link your app statically, all you have to do is to adjust the Qt path in `CMAKE_PREFIX_PATH`.
It has to point to a static Qt package which needs to be built in advance by your own (maybe it's available to download somewhere, or just ask me).

## Info for development

### Changing the number of static columns

E.g. the `Layer` column

Changing the offsets in the following functions:
- `MainWindow::loadSchematic`
- `MainWindow::attributesViewSelectionChanged`
- `EagleTableController::setSchematic`

## ToDo:

- removing an attribute and adding it again does not work
- values having two exponent specifier in them do evaluate both (eg the value of `100nF` will be `100e(-9+-12)` because `F` is interpreted as femto)