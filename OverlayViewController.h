#ifndef OVERLAYVIEWCONTROLLER_H
#define OVERLAYVIEWCONTROLLER_H

#include <QDebug>
#include <QEvent>
#include <QStackedLayout>
#include <QWidget>

class OverlayViewController : public QWidget {
    Q_OBJECT

    int m_shadowDistance;
    int m_shadowBlurRadius;

    void showOverlayWidget(QStackedLayout *l, QWidget *overlayWidget);

public:
    explicit OverlayViewController(QWidget *parent = nullptr);

    QWidget *baseWidget() const;
    void     setBaseWidget(QWidget *widget);
    QWidget *overlayWidget() const;
    void     setOverlayWidget(QWidget *overlayWidget);
    bool     hasOverlayWidget() const;
    bool     hasBaseWidget() const;

    void setShadowBlurRadius(int newValue) { m_shadowBlurRadius = newValue; }
    int  shadowBlurRadius() const { return m_shadowBlurRadius; }
    void setShadowDistance(int newValue) { m_shadowDistance = newValue; }
    int  shadowDistance() const { return m_shadowDistance; }

    void dismissOverlay();

    QSize sizeOfOverlayWithShadow() const {
        QSize size;
        if(QWidget *wdg = overlayWidget()) {
            size = wdg->size();
            size.setWidth(size.width() + 2 * m_shadowBlurRadius);
            size.setHeight(size.height() + m_shadowBlurRadius);
        }
        return size;
    }

signals:
    void animationFinished();

public slots:
};

#endif    // OVERLAYVIEWCONTROLLER_H
