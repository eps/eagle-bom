#include "SDCExportView.h"

#include <QComboBox>
#include <QDebug>
#include <QDir>
#include <QFileDialog>
#include <QHBoxLayout>
#include <QLabel>
#include <QPainterPath>
#include <QPushButton>
#include <QSpacerItem>
#include <QStandardPaths>

#include "AriesMCVSDCView.h"
#include "settings.h"

SDCExportView::SDCExportView(const eagle::Schematic *schematic, QWidget *parent) : QWidget(parent), m_schematic(schematic), m_sdcView(nullptr) {
    setSizePolicy(QSizePolicy::Policy::Fixed, QSizePolicy::Policy::Minimum);

    QGridLayout *gl = new QGridLayout(this);

    QHBoxLayout *top_hbl = new QHBoxLayout();

    QLabel *lbl = new QLabel("Select you SoM/SoC", this);
    top_hbl->addWidget(lbl);

    QComboBox *socSelectCB = new QComboBox(this);
    socSelectCB->addItems({ "Aries MCV SoM" });
    connect(socSelectCB, qOverload<int>(&QComboBox::currentIndexChanged), this, &SDCExportView::socSelectionChanged);
    top_hbl->addWidget(socSelectCB);

    top_hbl->addSpacerItem(new QSpacerItem(5, 5, QSizePolicy::MinimumExpanding, QSizePolicy::Fixed));

    gl->addLayout(top_hbl, 0, 0, 1, -1);

    QHBoxLayout *hbl = new QHBoxLayout();

    hbl->addSpacerItem(new QSpacerItem(5, 5, QSizePolicy::MinimumExpanding, QSizePolicy::Fixed));

    QPushButton *exportPB = new QPushButton("export", this);
    hbl->addWidget(exportPB);
    connect(exportPB, &QPushButton::released, this, &SDCExportView::exportReleased);

    QPushButton *cancelPB = new QPushButton("cancel", this);
    hbl->addWidget(cancelPB);
    connect(cancelPB, &QPushButton::released, this, &SDCExportView::dismiss);

    gl->addLayout(hbl, 2, 0, 1, -1);

    socSelectionChanged(0);
}

// QSize SDCExportView::sizeHint() const { return minimumSize(); }

void SDCExportView::keyReleaseEvent(QKeyEvent *event) {
    if (event->key() == Qt::Key_Escape) {
        emit dismiss();

        event->setAccepted(true);
    } else {
        QWidget::keyReleaseEvent(event);
    }
}

void SDCExportView::socSelectionChanged(int newIndex) {
    if (QGridLayout *l = qobject_cast<QGridLayout *>(layout())) {
        if (m_sdcView) {
            l->removeWidget(m_sdcView);
            delete m_sdcView;
            m_sdcView = nullptr;
        }

        switch (newIndex) {
            case 0: {
                m_sdcView = new AriesMCVSDCView(m_schematic, this);
                l->addWidget(m_sdcView, 1, 0);
                break;
            }
            default: qDebug() << __FUNCTION__ << newIndex << " not implemented";
        }
    }
}

void SDCExportView::exportReleased() {
    if (m_sdcView) {
        QString path;
        if (const std::string *lastOpenedFile = Settings::shared().recentFilesPeak()) {
            path = QString::fromStdString(*lastOpenedFile);
        } else {
            path = QStandardPaths::standardLocations(QStandardPaths::StandardLocation::HomeLocation).first();
        }
        QDir dir = QFileInfo(path).absoluteDir();

        QFileDialog fileDialog(this, "Set the filename of the SDC file", dir.path());
        fileDialog.setNameFilter("*.sdc");
        fileDialog.setViewMode(QFileDialog::Detail);
        fileDialog.setDefaultSuffix(".sdc");

        if (fileDialog.exec()) {
            QString filePath = fileDialog.selectedFiles().first();
            m_sdcView->exportToFile(filePath);
        }
    }
}

void SDCExportView::resizeEvent(QResizeEvent *) {
    const int radius = 10;
    auto	  path	 = QPainterPath();
    QRectF	  r		 = this->rect();
    r.adjust(0, 0, 0, radius);
    path.addRoundedRect(r, radius, radius);
    auto mask = QRegion(path.toFillPolygon().toPolygon());
    this->setMask(mask);
}
