#ifndef ABSTRACTSDCVIEW_H
#define ABSTRACTSDCVIEW_H

#include <QWidget>

#include "EagleLib/part.h"

class AbstractSDCView : public QWidget
{
    Q_OBJECT
public:
    explicit AbstractSDCView(QWidget *parent = nullptr);

    virtual void exportToFile(const QString& filename) = 0;



signals:

};

#endif // ABSTRACTSDCVIEW_H
