#ifndef ARIESMCVSDCVIEW_H
#define ARIESMCVSDCVIEW_H

#include <QMap>
#include <string>

#include <QComboBox>
#include <QDoubleSpinBox>
#include <QLineEdit>
#include <QWidget>

#include "AbstractSDCView.h"
#include "EagleLib/schematic.h"

class AriesMCVSDCView : public AbstractSDCView {
    Q_OBJECT
    const eagle::Schematic *		 m_schematic;
    std::vector<const eagle::Part *> m_parts;

    //    QComboBox *		m_hpsCB;
    QComboBox *		m_fpgaCB;
    QComboBox *		m_mcvVersionCB;
    QDoubleSpinBox *m_freqSB;
    QComboBox *		m_iovoltageCB;
    QLineEdit *		m_prefix;

    static QMap<std::string, QString> m_connectionTable_a2a4;
    static QMap<std::string, QString> m_connectionTable_a5a6;

public:
    explicit AriesMCVSDCView(const eagle::Schematic *schematic, QWidget *parent = nullptr);

    void exportToFile(const QString &filename);

signals:
};

#endif	// ARIESMCVSDCVIEW_H
