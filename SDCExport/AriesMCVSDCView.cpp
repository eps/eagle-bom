#include "AriesMCVSDCView.h"
#include <QDebug>
#include <QFile>
#include <QFileInfo>
#include <QFormLayout>
#include <QLabel>

QMap<std::string, QString> AriesMCVSDCView::m_connectionTable_a5a6 = {
    { "14", "W20" },   { "16", "W21" },	  { "18", "AA26" },	 { "20", "AB26" },	{ "22", "W24" },   { "24", "Y24" },	  { "28", "Y11" },	 { "30", "AA11" },
    { "32", "AD5" },   { "34", "AE6" },	  { "36", "AD4" },	 { "38", "AC4" },	{ "40", "AA4" },   { "42", "AB4" },	  { "46", "AF9" },	 { "48", "AE8" },
    { "50", "AF8" },   { "52", "AE7" },	  { "54", "AF4" },	 { "56", "AE4" },	{ "58", "AF6" },   { "60", "AF5" },	  { "64", "W11" },	 { "66", "V11" },
    { "68", "U11" },   { "70", "T11" },	  { "72", "W12" },	 { "74", "V12" },	{ "76", "T13" },   { "78", "T12" },	  { "82", "AH9" },	 { "84", "AG10" },
    { "88", "AH8" },   { "90", "AG9" },	  { "92", "AH7" },	 { "94", "AG8" },	{ "96", "AE15" },  { "98", "AF15" },  { "100", "AE17" }, { "102", "AD17" },
    { "106", "V13" },  { "108", "W14" },  { "110", "AA13" }, { "112", "Y13" },	{ "114", "AF13" }, { "116", "AG13" }, { "118", "AH12" }, { "120", "AF18" },
    { "124", "AG23" }, { "126", "AF23" }, { "128", "AF22" }, { "130", "AF21" }, { "132", "AF20" }, { "134", "AG20" }, { "136", "AE19" }, { "138", "AD19" },
    { "142", "AE23" }, { "144", "AE24" }, { "146", "AD20" }, { "148", "AE20" }, { "150", "U13" },  { "152", "U14" },  { "154", "AA15" }, { "156", "Y15" },
    { "160", "Y19" },  { "162", "AA20" }, { "164", "Y18" },	 { "166", "Y17" },	{ "168", "V15" },  { "170", "V16" },  { "172", "AD26" }, { "174", "AE25" },
    { "15", "D8" },	   { "17", "E8" },	  { "19", "D11" },	 { "21", "E11" },	{ "27", "T8" },	   { "29", "U9" },	  { "31", "V10" },	 { "33", "U10" },
    { "35", "Y8" },	   { "37", "W8" },	  { "39", "Y4" },	 { "41", "Y5" },	{ "45", "AH2" },   { "47", "AH3" },	  { "49", "AH4" },	 { "51", "AG5" },
    { "53", "AH5" },   { "55", "AH6" },	  { "57", "AG6" },	 { "59", "AF7" },	{ "63", "AE9" },   { "65", "AD10" },  { "67", "AF10" },	 { "69", "AF11" },
    { "71", "AD11" },  { "73", "AE11" },  { "75", "AD12" },	 { "77", "AE12" },	{ "81", "AH11" },  { "83", "AG11" },  { "87", "AH13" },	 { "89", "AG14" },
    { "91", "AH14" },  { "93", "AG15" },  { "95", "AG16" },	 { "97", "AF17" },	{ "99", "AH16" },  { "101", "AH17" }, { "105", "AH18" }, { "107", "AG18" },
    { "109", "AH19" }, { "111", "AG19" }, { "113", "AH21" }, { "115", "AG21" }, { "117", "AH22" }, { "119", "AH23" }, { "123", "AH24" }, { "125", "AG24" },
    { "127", "AG25" }, { "129", "AF25" }, { "131", "AH26" }, { "133", "AG26" }, { "135", "AH27" }, { "137", "AG28" }, { "141", "AF28" }, { "143", "AF27" },
    { "145", "AE22" }, { "147", "AD23" }, { "149", "AC23" }, { "151", "AC22" }, { "153", "AA18" }, { "155", "AA19" }, { "159", "AE26" }, { "161", "AF26" },
    { "163", "AB23" }, { "165", "AC24" }, { "167", "AA23" }, { "169", "AA24" }, { "171", "W15" },  { "173", "Y16" },  { "clk", "C12" },	 { "reset_n", "AB25" }
};
QMap<std::string, QString> AriesMCVSDCView::m_connectionTable_a2a4 = {
    { "14", "H6" },	   { "16", "H5" },	  { "18", "K8" },	 { "20", "L8" },	{ "22", "L9" },	   { "24", "L10" },	  { "28", "Y11" },	 { "30", "AA11" },
    { "32", "AD5" },   { "34", "AE6" },	  { "36", "AD4" },	 { "38", "AC4" },	{ "40", "AA4" },   { "42", "AB4" },	  { "46", "AF9" },	 { "48", "AE8" },
    { "50", "AF8" },   { "52", "AE7" },	  { "54", "AF4" },	 { "56", "AE4" },	{ "58", "AF6" },   { "60", "AF5" },	  { "64", "W11" },	 { "66", "V11" },
    { "68", "U11" },   { "70", "T11" },	  { "72", "W12" },	 { "74", "V12" },	{ "76", "T13" },   { "78", "T12" },	  { "82", "AH9" },	 { "84", "AG10" },
    { "88", "AH8" },   { "90", "AG9" },	  { "92", "AH7" },	 { "94", "AG8" },	{ "96", "AE15" },  { "98", "AF15" },  { "100", "AE17" }, { "102", "AD17" },
    { "106", "V13" },  { "108", "W14" },  { "110", "AA13" }, { "112", "Y13" },	{ "114", "AF13" }, { "116", "AG13" }, { "118", "AH12" }, { "120", "AF18" },
    { "124", "AG23" }, { "126", "AF23" }, { "128", "AF22" }, { "130", "AF21" }, { "132", "AF20" }, { "134", "AG20" }, { "136", "AE19" }, { "138", "AD19" },
    { "142", "AE23" }, { "144", "AE24" }, { "146", "AD20" }, { "148", "AE20" }, { "150", "U13" },  { "152", "U14" },  { "154", "AA15" }, { "156", "Y15" },
    { "160", "Y19" },  { "162", "AA20" }, { "164", "Y18" },	 { "166", "Y17" },	{ "168", "V15" },  { "170", "V16" },  { "172", "AD26" }, { "174", "AE25" },
    { "15", "D8" },	   { "17", "E8" },	  { "19", "D11" },	 { "21", "E11" },	{ "27", "T8" },	   { "29", "U9" },	  { "31", "V10" },	 { "33", "U10" },
    { "35", "Y8" },	   { "37", "W8" },	  { "39", "Y4" },	 { "41", "Y5" },	{ "45", "AH2" },   { "47", "AH3" },	  { "49", "AH4" },	 { "51", "AG5" },
    { "53", "AH5" },   { "55", "AH6" },	  { "57", "AG6" },	 { "59", "AF7" },	{ "63", "AE9" },   { "65", "AD10" },  { "67", "AF10" },	 { "69", "AF11" },
    { "71", "AD11" },  { "73", "AE11" },  { "75", "AD12" },	 { "77", "AE12" },	{ "81", "AH11" },  { "83", "AG11" },  { "87", "AH13" },	 { "89", "AG14" },
    { "91", "AH14" },  { "93", "AG15" },  { "95", "AG16" },	 { "97", "AF17" },	{ "99", "AH16" },  { "101", "AH17" }, { "105", "AH18" }, { "107", "AG18" },
    { "109", "AH19" }, { "111", "AG19" }, { "113", "AH21" }, { "115", "AG21" }, { "117", "AH22" }, { "119", "AH23" }, { "123", "AH24" }, { "125", "AG24" },
    { "127", "AG25" }, { "129", "AF25" }, { "131", "AH26" }, { "133", "AG26" }, { "135", "AH27" }, { "137", "AG28" }, { "141", "AF28" }, { "143", "AF27" },
    { "145", "AE22" }, { "147", "AD23" }, { "149", "AC23" }, { "151", "AC22" }, { "153", "AA18" }, { "155", "AA19" }, { "159", "AE26" }, { "161", "AF26" },
    { "163", "AB23" }, { "165", "AC24" }, { "167", "AA23" }, { "169", "AA24" }, { "171", "W15" },  { "173", "Y16" },  { "clk", "C12" },	 { "reset_n", "H4" }
};

AriesMCVSDCView::AriesMCVSDCView(const eagle::Schematic *schematic, QWidget *parent) : AbstractSDCView(parent), m_schematic(schematic) {
	QFormLayout *l = new QFormLayout(this);

    //    m_hpsCB		   = new QComboBox(this);
    m_fpgaCB	   = new QComboBox(this);
	m_mcvVersionCB = new QComboBox(this);
    m_iovoltageCB  = new QComboBox(this);

    m_prefix = new QLineEdit(this);

	m_freqSB = new QDoubleSpinBox(this);
	m_freqSB->setMaximum(10e9);
	m_freqSB->setMinimum(1.0);
	m_freqSB->setValue(50);
	m_freqSB->setSuffix(" MHz");
	m_freqSB->setDecimals(4);

	l->addRow("MCV version:", m_mcvVersionCB);
	l->addRow("Clock Frequency:", m_freqSB);
    l->addRow("IO Standard:", m_iovoltageCB);
    //	l->addRow("HPS Connector:", m_hpsCB);
	l->addRow("FPGA Connector:", m_fpgaCB);
    l->addRow("Optional Prefix:", m_prefix);

	schematic->partsWithPinCount(180, m_parts);

    m_mcvVersionCB->addItems({ "A2/A4", "A5/A6" });
    m_iovoltageCB->addItems({ "2.5V", "1.8V", "1.5V", "1.2V", "3.3-V LVTTL", "3.3-V LVCMOS", "3.0-V LVTTL", "3.0-V LVCMOS" });
    m_iovoltageCB->setCurrentIndex(4);

	for (const eagle::Part *p : m_parts) {
		QString item = QString::fromStdString(p->name());
        //		m_hpsCB->addItem(item);
		m_fpgaCB->addItem(item);
	}
}

/**
 * @brief AriesMCVSDCView::exportToFile
 * todo: this version only works when the connector is a single gate
 * @param filename
 */
void AriesMCVSDCView::exportToFile(const QString &sdcFilename) {
	if (const eagle::Part *p = eagle::Schematic::findPart(m_fpgaCB->currentText().toStdString(), m_parts)) {
		const std::vector<const eagle::Gate *> &gates = p->device()->deviceset()->gates();
        if (!sdcFilename.isEmpty()) {
            QFile file(sdcFilename);
            if (file.open(QIODevice::OpenModeFlag::WriteOnly | QIODevice::OpenModeFlag::Truncate)) {
                file.write("# Setup clock\n");
                double	clk	   = 1000.0 / m_freqSB->value();
                QString clkStr = QString("create_clock -period %1 -name clk -waveform {0.000 %2} -add [get_ports clk]\n")
                                     .arg(QString::number(clk, 'f', 3))
                                     .arg(QString::number(clk / 2, 'f', 3));
                file.write(clkStr.toUtf8());
            }

            QFileInfo info(sdcFilename);
            QString	  tclFilename = info.path() + "/" + info.completeBaseName() + ".tcl";
            QFile	  tclfile(tclFilename);
            if (tclfile.open(QIODevice::OpenModeFlag::WriteOnly | QIODevice::OpenModeFlag::Truncate)) {
                if (m_mcvVersionCB->currentIndex() == 0) {	// a2/a4

                    // add clock and reset signals
                    QString clkName = "clk";
                    QString str =
                        QString("set_location_assignment PIN_%1 -to %2\n").arg(AriesMCVSDCView::m_connectionTable_a2a4[clkName.toStdString()]).arg(clkName);
                    str += QString("set_instance_assignment -name IO_STANDARD \"%1\" -to %2\n").arg(m_iovoltageCB->currentText()).arg(clkName);
                    QString resetName = "reset_n";
                    str +=
                        QString("set_location_assignment PIN_%1 -to %2\n").arg(AriesMCVSDCView::m_connectionTable_a2a4[resetName.toStdString()]).arg(resetName);
                    str += QString("set_instance_assignment -name IO_STANDARD \"%1\" -to %2\n").arg(m_iovoltageCB->currentText()).arg(resetName);
                    tclfile.write(str.toUtf8());

                    for (auto g : gates) {
                        if (const eagle::Sheet *s = m_schematic->sheetForPart(p, g)) {
                            std::vector<const eagle::Net *> nets;
                            s->nets(p, g, nets);
                            for (const eagle::Net *n : nets) {
                                if (const eagle::Pin *pin = n->connects(p, g)) {
                                    if (AriesMCVSDCView::m_connectionTable_a2a4.count(pin->name())) {
                                        QString pinName = QString::fromStdString(n->name()).toLower();
                                        if (pinName.startsWith("!")) pinName = pinName.remove(0, 1) + "_n";
                                        pinName		= m_prefix->text() + pinName;
                                        QString str = QString("set_location_assignment PIN_%1 -to %2\n")
                                                          .arg(AriesMCVSDCView::m_connectionTable_a2a4[pin->name()])
                                                          .arg(pinName);
                                        str +=
                                            QString("set_instance_assignment -name IO_STANDARD \"%1\" -to %2\n").arg(m_iovoltageCB->currentText()).arg(pinName);

                                        tclfile.write(str.toUtf8());
                                    }
                                }
                            }
                        }
                    }
                } else {  // a5/a6

                    // add clock and reset signals
                    QString clkName = "clk";
                    QString str =
                        QString("set_location_assignment PIN_%1 -to %2\n").arg(AriesMCVSDCView::m_connectionTable_a5a6[clkName.toStdString()]).arg(clkName);
                    str += QString("set_instance_assignment -name IO_STANDARD \"%1\" -to %2\n").arg(m_iovoltageCB->currentText()).arg(clkName);
                    QString resetName = "reset_n";
                    str +=
                        QString("set_location_assignment PIN_%1 -to %2\n").arg(AriesMCVSDCView::m_connectionTable_a5a6[resetName.toStdString()]).arg(resetName);
                    str += QString("set_instance_assignment -name IO_STANDARD \"%1\" -to %2\n").arg(m_iovoltageCB->currentText()).arg(resetName);
                    tclfile.write(str.toUtf8());

                    for (auto g : gates) {
                        if (const eagle::Sheet *s = m_schematic->sheetForPart(p, g)) {
                            std::vector<const eagle::Net *> nets;
                            s->nets(p, g, nets);
                            for (const eagle::Net *n : nets) {
                                if (const eagle::Pin *pin = n->connects(p, g)) {
                                    if (AriesMCVSDCView::m_connectionTable_a5a6.count(pin->name())) {
                                        QString pinName = QString::fromStdString(n->name()).toLower();
                                        if (pinName.startsWith("!")) pinName = pinName.remove(0, 1) + "_n";
                                        pinName		= m_prefix->text() + pinName;
                                        QString str = QString("set_location_assignment PIN_%1 -to %2\n")
                                                          .arg(AriesMCVSDCView::m_connectionTable_a5a6[pin->name()])
                                                          .arg(pinName);
                                        str +=
                                            QString("set_instance_assignment -name IO_STANDARD \"%1\" -to %2\n").arg(m_iovoltageCB->currentText()).arg(pinName);

                                        tclfile.write(str.toUtf8());
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
