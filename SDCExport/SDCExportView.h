#ifndef SDCEXPORTVIEW_H
#define SDCEXPORTVIEW_H

#include <QGridLayout>
#include <QKeyEvent>
#include <QWidget>

#include "AbstractSDCView.h"
#include "EagleLib/schematic.h"

class SDCExportView : public QWidget {
    Q_OBJECT
    const eagle::Schematic* m_schematic;
    AbstractSDCView*		m_sdcView;

public:
    explicit SDCExportView(const eagle::Schematic* schematic, QWidget* parent = nullptr);

    //    QSize sizeHint() const override;

    void resizeEvent(QResizeEvent* event) override;
    void keyReleaseEvent(QKeyEvent* event) override;

signals:
    void dismiss();

public slots:
    void socSelectionChanged(int newIndex);
    void exportReleased();
};

#endif	// SDCEXPORTVIEW_H
