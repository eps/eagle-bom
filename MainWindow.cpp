#include "MainWindow.h"

#include "EagleLib/schematic.h"
#include "settings.h"

// #include "SDCExport/SDCExportView.h"

#include <QApplication>
#include <QCheckBox>
#include <QClipboard>
#include <QDebug>
#include <QDesktopServices>
#include <QDockWidget>
#include <QFile>
#include <QFileDialog>
#include <QHeaderView>
#include <QMenuBar>
#include <QMimeData>
#include <QStandardPaths>
#include <QTextBrowser>
#include <QWidgetAction>

MainWindow::MainWindow(QWidget* parent) : QMainWindow(parent), m_tableView(nullptr) {
    createMenu();

    setAcceptDrops(true);

    m_backgroundInfoView = new BackgroundInfoWidget(this);
    connect(m_backgroundInfoView, &BackgroundInfoWidget::handleOpen, this, &MainWindow::backgroundInfoViewHandleOpen);

    // Setup central widget as stacked widget
    m_overlayVC = new OverlayViewController(this);
    m_overlayVC->setBaseWidget(m_backgroundInfoView);

    this->setCentralWidget(m_overlayVC);

    // create Filter Widget as a Docking Widget
    QDockWidget* dock = new QDockWidget("Part Filter", this);
    dock->setAllowedAreas(Qt::DockWidgetArea::LeftDockWidgetArea | Qt::DockWidgetArea::RightDockWidgetArea);
    m_viewMenu->addAction(dock->toggleViewAction());
    m_filterView = new FilterView(this);

    dock->setWidget(m_filterView);
    addDockWidget(Qt::DockWidgetArea::RightDockWidgetArea, dock);

    connect(m_filterView, &FilterView::selectionChanged, this, &MainWindow::filterViewSelectionChanged);
    connect(m_filterView, &FilterView::showBoardLayersToggled, this, &MainWindow::filterViewShowBoardLayersToggled);
    connect(m_filterView, &FilterView::searchTextChanged, this, &MainWindow::filterViewSearchTextChanged);

    // create Attributes ListView as a Docking Widget
    dock = new QDockWidget("Attributes", this);
    dock->setAllowedAreas(Qt::DockWidgetArea::LeftDockWidgetArea | Qt::DockWidgetArea::RightDockWidgetArea);
    m_viewMenu->addAction(dock->toggleViewAction());
    m_attributesView = new AttributesView(this);
    dock->setWidget(m_attributesView);
    addDockWidget(Qt::DockWidgetArea::RightDockWidgetArea, dock);

    connect(m_attributesView, &AttributesView::selectionChanged, this, &MainWindow::attributesViewSelectionChanged);
    connect(m_attributesView, &AttributesView::attributeAdded, this, &MainWindow::attributesViewAttributeAdded);
    connect(m_attributesView, &AttributesView::attributeDeleted, this, &MainWindow::attributesViewAttributeDeleted);

    if (const std::string* lastOpenedFile = Settings::shared().recentFilesPeak()) {
        const QString fileName = QString::fromStdString(*lastOpenedFile);
        if (QFile(fileName).exists()) { loadSchematic(fileName, true); }
        else{
            Settings::shared().recentFilesRemove(*lastOpenedFile);
        }
    }
}

void MainWindow::dragEnterEvent(QDragEnterEvent* event) {
    if (event->mimeData()->hasUrls() && event->mimeData()->urls().count() == 1) {
        QUrl      file = event->mimeData()->urls().first();
        QFileInfo info(file.toString(QUrl::UrlFormattingOption::PreferLocalFile));
        if (info.suffix() == "sch") event->acceptProposedAction();
    }
}

void MainWindow::dropEvent(QDropEvent* event) {
    QUrl url = event->mimeData()->urls().first();
    if (url.isValid() && url.isLocalFile()) {
        QString file = url.toString(QUrl::UrlFormattingOption::PreferLocalFile);
        loadSchematic(file, false);
    }
}

void MainWindow::loadSchematic(const QString& schematic_path, bool reload_settings) {
    if (!m_tableView) {
        m_tableView = new TableView(this);
        m_tableView->setSelectionMode(QAbstractItemView::SelectionMode::SingleSelection);

        m_tableController      = new EagleTableController(schematic_path, m_tableView);
        m_tableControllerProxy = new EagleTableControllerProxy(m_tableView);
        connect(m_tableController, &EagleTableController::forceUpdate, m_tableControllerProxy,
                &EagleTableControllerProxy::invalidate);
        m_tableControllerProxy->setSourceModel(m_tableController);
        m_tableView->setModel(m_tableControllerProxy);

        m_tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Interactive);
        m_tableView->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeMode::ResizeToContents);
        m_tableView->setSortingEnabled(true);
        m_overlayVC->setBaseWidget(m_tableView);

        m_tableView->setContextMenuPolicy(Qt::CustomContextMenu);
        connect(m_tableView, &QTableView::customContextMenuRequested, this, &MainWindow::tableViewShowContextMenu);
    } else {
        m_tableController->setSchematic(schematic_path);
        // m_tableControllerProxy->invalidate();
    }

    // save currently selected attributes so they can be selected again once the schematic has been reloaded
    std::list<std::string> selectedAttributes;
    if (!reload_settings) { m_attributesView->selectedAttributes(selectedAttributes); }

    // Refresh attributes from the schematic in the attributesView
    // Clear and reload again in case the reloaded schematic has changed some attributes
    m_attributesView->setAttributes(m_tableController->attributes());

    // Select/Show all attributes that have been visible last time
    const int numBaseColumns = m_tableController->numBaseColumns() + 1;  // +1 due to the layer column
    const std::list<std::string>& visibleAttributes =
        (reload_settings) ? Settings::shared().getVisibleAttributes() : selectedAttributes;
    for (int i = 0; i < m_attributesView->attributesCount(); i++) {
        const std::string text = m_attributesView->attributeForIndex(i);
        if (std::find(visibleAttributes.begin(), visibleAttributes.end(), text) != visibleAttributes.end()) {
            // unhiding is done in the selectionChanged callback, so no need for setColumnHidden(_, false) here
            m_attributesView->select(i, QItemSelectionModel::SelectionFlag::Select);
        } else {
            m_tableView->setColumnHidden(numBaseColumns + i, true);
        }
    }

    if (reload_settings) {
        m_filterView->setSelection(Settings::shared().selectedFilterTypes());
        m_filterView->setBoardLayers(Settings::shared().viewBoardLocations());

        bool viewGroupedParts = Settings::shared().viewGroupedParts();
        m_viewMenuGroupedAction->setChecked(viewGroupedParts);
        viewMenuGroupedAction(viewGroupedParts);  // does handle the sorting
    } else {
        int columnToSort = m_tableController->grouping() ? m_tableController->indexOfColumn("Value") :
                                                           m_tableController->indexOfColumn("Part");
        if (columnToSort >= 0) { m_tableView->sortByColumn(columnToSort, Qt::SortOrder::AscendingOrder); }
    }

    Settings::shared().recentFilesPush(schematic_path.toStdString());

    setWindowTitle(QString("eagle-bom - ") + schematic_path);
}

// void MainWindow::mouseReleaseEvent(QMouseEvent* event) {
//	if (event->button() == Qt::MouseButton::RightButton) {
//        qDebug() << __func__;
//		QModelIndex proxyIndex = m_tableView->indexAt(event->pos());
//		if (proxyIndex.isValid()) {
//			if (m_viewMenuGroupedAction) {
//                const QModelIndex  sourceIndex = m_valueProxy->mapToSource(proxyIndex);
//                const eagle::Part* p		   = m_valueModel->keyForRow(sourceIndex.row());
//				qDebug() << __FUNCTION__ << ": index: " << QString::fromStdString(p->name());
//			} else {
//                const QModelIndex  sourceIndex = m_nameProxy->mapToSource(proxyIndex);
//                const eagle::Part* p		   = m_nameModel->partForRow(sourceIndex.row());
//				qDebug() << __FUNCTION__ << ": index: " << QString::fromStdString(p->name());
//			}
//		}
//	}
//}

void MainWindow::tableViewShowContextMenu(const QPoint& pos) {
    QModelIndex proxyIndex = m_tableView->indexAt(pos);

    if (proxyIndex.isValid()) {
        QPoint globalClickPos = m_tableView->mapToGlobal(pos);
        globalClickPos += QPoint(m_tableView->verticalHeader()->width(), m_tableView->horizontalHeader()->height());

        const QModelIndex  sourceIndex = m_tableControllerProxy->mapToSource(proxyIndex);
        const eagle::Part* part        = m_tableController->partAt(sourceIndex.row());
        QMenu              menu("Context Menu", this);
        bool               showMenu = false;
        if (part->hasDatasheet()) {
            menu.addAction("Open Datasheet", [&]() {
                const QString datasheet = QString::fromStdString(part->datasheet());
                QDesktopServices::openUrl(QUrl(datasheet));
            });
            showMenu = true;
        }
        if (m_tableController->columnAt(sourceIndex.column()).rfind("Part", 0) == 0) {
            QString menuName = (m_tableController->grouping()) ?
                                   "Copy \"show @ [parts]\"" :
                                   QString("Copy \"show @ %1\"").arg(QString::fromStdString(part->name()));
            menu.addAction(menuName, [&]() {
                auto partsStr = m_tableController->data(sourceIndex, Qt::DisplayRole).toString().replace(",", "");
                QClipboard* clipboard = QGuiApplication::clipboard();
                clipboard->setText(QString("show @ %1").arg(partsStr));
            });
            showMenu = true;
        }
        if (showMenu) menu.exec(globalClickPos);
    }
}

void MainWindow::createMenu() {
    // file
    m_fileMenu = menuBar()->addMenu("File");

    m_fileMenuOpenAction = new QAction("Open", this);
    m_fileMenuOpenAction->setShortcut(QKeySequence::Open);
    connect(m_fileMenuOpenAction, &QAction::triggered, this, &MainWindow::fileMenuOpenAction);
    m_fileMenu->addAction(m_fileMenuOpenAction);

    m_fileMenuRecentFilesMenu = m_fileMenu->addMenu("Recent Files");
    connect(m_fileMenuRecentFilesMenu, &QMenu::aboutToShow, this, &MainWindow::fileMenuRecentFilesAboutToShow);

    m_fileMenuReloadAction = new QAction("Reload", this);
    m_fileMenuReloadAction->setShortcut(QKeySequence(Qt::Modifier::CTRL | Qt::Key::Key_R));
    connect(m_fileMenuReloadAction, &QAction::triggered, this, &MainWindow::fileMenuReloadAction);
    m_fileMenu->addAction(m_fileMenuReloadAction);

    m_fileMenuRevealInExplorerAction = new QAction("Reveal in Explorer", this);
    m_fileMenuRevealInExplorerAction->setShortcut(
        QKeySequence(Qt::Modifier::SHIFT | Qt::Modifier::ALT | Qt::Key::Key_R));
    connect(m_fileMenuRevealInExplorerAction, &QAction::triggered, this, &MainWindow::fileMenuRevealInExplorerAction);
    m_fileMenu->addAction(m_fileMenuRevealInExplorerAction);

    // file - export
    m_fileExportMenu = m_fileMenu->addMenu("Export");

    m_fileExportMenuBOMAction = new QAction("BOM", this);
    connect(m_fileExportMenuBOMAction, &QAction::triggered, this, &MainWindow::fileExportMenuBOMAction);
    m_fileExportMenu->addAction(m_fileExportMenuBOMAction);

    m_fileExportMenuSMDMountingAction = new QAction("SMD Mounting", this);
    connect(m_fileExportMenuSMDMountingAction, &QAction::triggered, this, &MainWindow::fileExportMenuSMDMountingAction);
    m_fileExportMenu->addAction(m_fileExportMenuSMDMountingAction);

    m_fileExportMenuEagleChangeScriptAction = new QAction("Generate Change Script (.scr)", this);
    connect(m_fileExportMenuEagleChangeScriptAction, &QAction::triggered, this,
            &MainWindow::fileExportMenuEagleChangeScriptAction);
    m_fileExportMenu->addAction(m_fileExportMenuEagleChangeScriptAction);

    m_fileMenuQuitAction = new QAction("Quit", this);
    m_fileMenuQuitAction->setShortcut(QKeySequence(Qt::Modifier::CTRL | Qt::Key::Key_Q));
    connect(m_fileMenuQuitAction, &QAction::triggered, this, &MainWindow::fileMenuQuitAction);
    m_fileMenu->addAction(m_fileMenuQuitAction);

    // edit
    m_editMenu = menuBar()->addMenu("Edit");

    m_editMenuUndoAction = new QAction("Undo", this);
    m_editMenuUndoAction->setShortcut(QKeySequence::Undo);
    connect(m_editMenuUndoAction, &QAction::triggered, this, &MainWindow::editMenuUndoAction);
    m_editMenu->addAction(m_editMenuUndoAction);

    m_editMenuRedoAction = new QAction("Redo", this);
    m_editMenuRedoAction->setShortcut(QKeySequence::Redo);
    connect(m_editMenuRedoAction, &QAction::triggered, this, &MainWindow::editMenuRedoAction);
    m_editMenu->addAction(m_editMenuRedoAction);

    // view
    m_viewMenu = menuBar()->addMenu("View");

    m_viewMenuGroupedAction = new QAction("Group by Value", this);
    m_viewMenuGroupedAction->setCheckable(true);
    m_viewMenuGroupedAction->setShortcut(QKeySequence(Qt::Modifier::CTRL | Qt::Key::Key_G));
    connect(m_viewMenuGroupedAction, &QAction::triggered, this, &MainWindow::viewMenuGroupedAction);
    m_viewMenu->addAction(m_viewMenuGroupedAction);

    // Eagle-BOM
    m_toolMenu = menuBar()->addMenu("Tool");

    m_toolMenuAboutAction = new QAction("About", this);
    connect(m_toolMenuAboutAction, &QAction::triggered, this, &MainWindow::toolMenuAboutAction);
    m_toolMenu->addAction(m_toolMenuAboutAction);

    // utility
    // m_utilityMenu = menuBar()->addMenu("Utility");

    // m_utilityMenuExportSDC = new QAction("Generate SDC File", this);
    // connect(m_utilityMenuExportSDC, &QAction::triggered, this, &MainWindow::utilityMenuExportSDCAction);
    // m_utilityMenu->addAction(m_utilityMenuExportSDC);
}
void MainWindow::fileMenuOpenAction(bool /*checked*/) {
    QString path;
    if (const std::string* lastOpenedFile = Settings::shared().recentFilesPeak()) {
        path = QString::fromStdString(*lastOpenedFile);
    } else {
        path = QStandardPaths::standardLocations(QStandardPaths::StandardLocation::HomeLocation).first();
    }
    QDir dir = QFileInfo(path).absoluteDir();

    QFileDialog fileDialog(this, "Select the Schematic file", dir.path());
    fileDialog.setNameFilter("*.sch");
    fileDialog.setFileMode(QFileDialog::ExistingFile);
    fileDialog.setViewMode(QFileDialog::Detail);

    if (fileDialog.exec()) {
        QString filePath = fileDialog.selectedFiles().first();
        loadSchematic(filePath, false);
    }
}

void MainWindow::fileMenuAboutToShow() {
    m_fileMenuRevealInExplorerAction->setIconVisibleInMenu(centralWidget() == m_tableView);
}

void MainWindow::fileMenuRevealInExplorerAction(bool /*checked*/) {
    if (const std::string* recentFile = Settings::shared().recentFilesPeak()) {
        std::string openFile(*recentFile);
        QUrl schematic(QUrl(QString::fromStdString(openFile)).toString(QUrl::UrlFormattingOption::RemoveFilename));
        QDesktopServices::openUrl(schematic);
    }
}

void MainWindow::fileMenuRecentFilesAboutToShow() {
    m_fileMenuRecentFilesMenu->clear();
    for (const std::string& file : (Settings::shared().getRecentFiles())) {
        m_fileMenuRecentFilesMenu->addAction(QString::fromStdString(file), [this, file](bool) {
            this->loadSchematic(QString::fromStdString(file), false);
        });
    }
}

void MainWindow::fileMenuReloadAction(bool /*checked*/) {
    loadSchematic(QString::fromStdString(*Settings::shared().recentFilesPeak()), false);
}

void MainWindow::fileExportMenuBOMAction(bool /*checked*/) {
    QString openPath = QStandardPaths::writableLocation(QStandardPaths::DesktopLocation);
    if (const std::string* openFile = Settings::shared().recentFilesPeak()) {
        QUrl      path(QString::fromStdString(*openFile));
        QString   filepath = path.toString();
        QFileInfo info     = QFileInfo(filepath);
        openPath           = info.canonicalPath() + "/" + info.completeBaseName() + "-bom";
    }
    QString saveFile = QFileDialog::getSaveFileName(this, "Choose file", openPath, "CSV (*.csv)");
    if (!saveFile.isEmpty()) {
        QFile file(saveFile);
        if (file.open(QIODevice::OpenModeFlag::WriteOnly | QIODevice::OpenModeFlag::Truncate)) {
            QStringList headers;
            QList<int>  headerIndices;
            for (int i = 0; i < m_tableControllerProxy->columnCount(); i++) {
                if (!m_tableView->isColumnHidden(i)) {
                    headerIndices.append(i);
                    headers.append(
                        m_tableControllerProxy->headerData(i, Qt::Orientation::Horizontal, Qt::DisplayRole).toString());
                }
            }

            QString headerStr = QString(headers.join(";") + "\n");
            file.write(headerStr.toUtf8());

            for (int r = 0; r < m_tableControllerProxy->rowCount(); r++) {
                if (!m_tableView->isRowHidden(r)) {
                    QString str;
                    for (int c : headerIndices) {
                        str += m_tableControllerProxy->data(m_tableControllerProxy->index(r, c), Qt::DisplayRole)
                                   .toString();
                        str += ";";
                    }
                    str += m_tableControllerProxy
                               ->data(m_tableControllerProxy->index(r, m_tableControllerProxy->columnCount() - 1),
                                      Qt::DisplayRole)
                               .toString();
                    str += "\n";
                    file.write(str.toUtf8());
                }
            }
        } else {
            qDebug() << QString(__FUNCTION__) + " failed to open file: " + saveFile;
        }
    }
}

void MainWindow::fileExportMenuSMDMountingAction(bool /*checked*/) {
    QString openPath = QStandardPaths::writableLocation(QStandardPaths::DesktopLocation);
    if (const std::string* openFile = Settings::shared().recentFilesPeak()) {
        QUrl      path(QString::fromStdString(*openFile));
        QString   filepath = path.toString();
        QFileInfo info     = QFileInfo(filepath);
        openPath           = info.canonicalPath() + "/" + info.completeBaseName() + "-cpl";
    }
    QString saveFile = QFileDialog::getSaveFileName(this, "Choose file", openPath, "CSV (*.csv)");
    if (!saveFile.isEmpty()) {
        QFile file(saveFile);
        if (file.open(QIODevice::OpenModeFlag::WriteOnly | QIODevice::OpenModeFlag::Truncate)) {
            QString headers("Part Name;X;Y;Rotation;Side");
            file.write(headers.toUtf8());
            auto parts = m_tableController->parts();
            for (auto part : parts) {
                if (part->hasFootprint()) {
                    std::string s = part->name() + ";" + std::to_string(part->boardLocation().x()) + ";" +
                                    std::to_string(part->boardLocation().y()) + ";" +
                                    std::to_string(part->boardLocation().rotation()) + ";" +
                                    (part->boardLocation().bottom() ? "Bottom" : "Top") + "\n";
                    file.write(QString::fromStdString(s).toUtf8());
                }
            }
        }
    } else {
        qDebug() << __func__ << " failed to open file: " + saveFile;
    }
}

void MainWindow::fileExportMenuEagleChangeScriptAction(bool /*checked*/) {
    QString openPath = QStandardPaths::writableLocation(QStandardPaths::DesktopLocation);
    if (const std::string* openFile = Settings::shared().recentFilesPeak()) {
        QUrl      path(QString::fromStdString(*openFile));
        QString   filepath = path.toString();
        QFileInfo info     = QFileInfo(filepath);
        openPath           = info.canonicalPath() + "/" + info.completeBaseName();

        // find every part from current container (m_tableController->parts()) in originalParts and compare them by
        // value
    }

    QString saveFile = QFileDialog::getSaveFileName(this, "Choose file", openPath, "Eagle Script (*.scr)");
    if (!saveFile.isEmpty()) {
        QFile file(saveFile);
        if (file.open(QIODevice::OpenModeFlag::WriteOnly | QIODevice::OpenModeFlag::Truncate)) {
            file.write(QString("change display off;\n").toUtf8());
            // std::list<std::string> activeAtributes;
            // attributesViewSelectedAttributes(activeAtributes);
            for (const eagle::Part* part : m_tableController->parts()) {
                if (auto originalPart = eagle::Schematic::findPart(part->name(), m_tableController->originalParts())) {
                    std::string changes;
                    bool        wrotePage = false;

                    if (part->value(false) != originalPart->value(false)) {
                        if (!wrotePage) {
                            const eagle::Sheet* s = m_tableController->sheetForPart(part);
                            changes += std::string("edit .s") + std::to_string(s->number()) + "\n";
                            wrotePage = true;
                        }
                        changes += "value " + part->name() + " '" + part->value(false) + "';\n";
                    }

                    // assess every attribute
                    for (const std::string& key : m_tableController->columns()) {
                        const std::string& partAttr = part->hasAttribute(key) ? part->attribute(key) : "";
                        const std::string& origAttr =
                            originalPart->hasAttribute(key) ? originalPart->attribute(key) : "";
                        if (partAttr != origAttr) {
                            if (!wrotePage) {
                                const eagle::Sheet* s = m_tableController->sheetForPart(part);
                                changes += std::string("edit .s") + std::to_string(s->number()) + "\n";
                                wrotePage = true;
                            }
                            if (partAttr.empty()) {
                                changes += "attribute " + part->name() + " " + key + " delete\n";
                            } else {
                                changes += "attribute " + part->name() + " " + key + " '" + partAttr + "';\n";
                            }
                        }
                    }
                    file.write(QString::fromStdString(changes).toUtf8());
                }
            }
        }
    }
}

void MainWindow::fileMenuQuitAction(bool /*checked*/) {
    std::list<std::string> selectedAttributes;
    m_attributesView->selectedAttributes(selectedAttributes);
    Settings::shared().setVisibleAttributes(selectedAttributes);
    std::vector<int> selectedTypes;
    m_filterView->selectedFilterTypes(selectedTypes);
    Settings::shared().setSelectedFilterTypes(selectedTypes);
    Settings::shared().setViewBoardLocations(m_filterView->showBoardLayers());
    Settings::shared().save();
    QApplication::instance()->quit();
}

void MainWindow::editMenuUndoAction(bool /*checked*/) {
    m_tableController->undo();
    m_tableControllerProxy->invalidate();
}

void MainWindow::editMenuRedoAction(bool /*checked*/) {
    m_tableController->redo();
    m_tableControllerProxy->invalidate();
}

void MainWindow::viewMenuGroupedAction(bool checked) {
    m_tableView->selectionModel()->clearSelection();
    int indexOfValueColumn = -1;
    if (!checked) {
        // restore state of n-th column to be interactive again
        int partsIndex = m_tableController->indexOfColumn("Parts");
        if (partsIndex >= 0) {
            m_tableView->horizontalHeader()->setSectionResizeMode(partsIndex, QHeaderView::ResizeMode::Interactive);
        }
        m_tableController->setGrouping(checked);
        indexOfValueColumn = m_tableController->indexOfColumn("Part");
    } else {
        m_tableController->setGrouping(checked);
        int partsIndex = m_tableController->indexOfColumn("Parts");
        m_tableView->horizontalHeader()->setSectionResizeMode(partsIndex, QHeaderView::ResizeMode::Stretch);
        indexOfValueColumn = m_tableController->indexOfColumn("Value");
    }
    if (indexOfValueColumn >= 0) { m_tableView->sortByColumn(indexOfValueColumn, Qt::SortOrder::AscendingOrder); }
    m_tableControllerProxy->invalidate();
    filterViewSearchTextChanged(m_filterView->searchText());
    Settings::shared().setViewGroupedParts(checked);
}

void MainWindow::toolMenuAboutAction(bool /*checked*/) {
    QWidget* view = new QWidget(this);
    view->setFixedSize(480, 270);
    QVBoxLayout*  vbl = new QVBoxLayout(view);
    QTextBrowser* te  = new QTextBrowser(this);
    te->setOpenExternalLinks(true);
    te->setOpenLinks(true);
    te->setHtml(
        "Written by Tim Kruse (t.kruse@gsi.de)<br>"
        "Report bugs at <a href=\"https://git.gsi.de/t.-h.kruse/eagle-bom/-/issues\">https://git.gsi.de/t.-h.kruse/eagle-bom/-/issues</a><br><br>"
        "This tool stores its persistent settings at:<br>%APPDATA%\\eagle-bom<br><br>"
        "Credits:<br>"
        "QT 5.15: <a href=\"https://www.qt.io/\">https://www.qt.io/</a><br>"
        "JSON Lib: <a href=\"https://github.com/nlohmann/json\">https://github.com/nlohmann/json</a><br>"
        "XML Parser lib:<a href=\"https://github.com/discord/rapidxml/blob/master/rapidxml.hpp\">https://github.com/discord/rapidxml/blob/master/rapidxml.hpp</a><br>"
        "Eagle Cpp Lib: <a href=\"https://git.gsi.de/t.-h.kruse/eagle-cpp-lib\">https://git.gsi.de/t.-h.kruse/eagle-cpp-lib</a>");
    vbl->addWidget(te);
    QPushButton* okBtn = new QPushButton("Ok, cool ...", view);
    vbl->addWidget(okBtn);
    view->adjustSize();
    connect(okBtn, &QPushButton::released, m_overlayVC, &OverlayViewController::dismissOverlay);
    m_overlayVC->setOverlayWidget(view);
}
// void MainWindow::utilityMenuExportSDCAction(bool /*checked*/) {
//     SDCExportView* view = new SDCExportView(m_schematic, this);
//     view->adjustSize();
//     connect(view, &SDCExportView::dismiss, m_overlayVC, &OverlayViewController::dismissOverlay);
//     m_overlayVC->setOverlayWidget(view);
// }

void MainWindow::backgroundInfoViewHandleOpen() { fileMenuOpenAction(false); }

void MainWindow::attributesViewSelectionChanged(const QItemSelection& selected, const QItemSelection& deselected) {
    const int numBaseColumns = m_tableController->numBaseColumns() + 1;  // +1 due to the layer column
    // if (m_filterView->showBoardLayers()) numBaseColumns++;

    const QModelIndexList& selectedIndexes = selected.indexes();
    for (QModelIndexList::const_iterator it = selectedIndexes.begin(); it != selectedIndexes.end(); it++) {
        const QModelIndex& index = *it;
        m_tableView->setColumnHidden(numBaseColumns + index.row(), false);
    }

    const QModelIndexList& deselectedIndexes = deselected.indexes();
    for (QModelIndexList::const_iterator it = deselectedIndexes.begin(); it != deselectedIndexes.end(); it++) {
        const QModelIndex& index = *it;
        m_tableView->setColumnHidden(numBaseColumns + index.row(), true);
    }
}

void MainWindow::filterViewSelectionChanged(const QItemSelection& selected, const QItemSelection& deselected) {
    uint32_t typeFilter = m_tableControllerProxy->typeFilter();
    for (auto index : selected.indexes()) { typeFilter |= (1 << index.row()); }
    for (auto index : deselected.indexes()) { typeFilter &= ~(1 << index.row()); }
    m_tableControllerProxy->setTypeFilter(typeFilter);
}

void MainWindow::filterViewShowBoardLayersToggled(bool checked) {
    int index = m_tableController->indexOfColumn("Layer");
    m_tableView->setColumnHidden(index, !checked);
}
void MainWindow::filterViewSearchTextChanged(QString newText) {
    QList<int> visibleColumnIndices;
    for (int i = 0; i < m_tableController->columnCount(); i++) {
        if (!m_tableView->isColumnHidden(i)) {
            if (m_tableController->columnAt(i) != "Qty") visibleColumnIndices.append(i);
        }
    }

    // QStringList cols;
    // for (auto i : visibleColumnIndices) { cols.append(QString::fromStdString(m_tableController->columnAt(i))); }

    // qDebug() << __func__ << cols;

    m_tableControllerProxy->setSearchRegex(newText, visibleColumnIndices);
}

void MainWindow::attributesViewAttributeAdded(QString attr, int index) {
    const int         colToAdd = m_tableController->numBaseColumns() + (1) + index;  // +1 due to the layers column
    const std::string colName  = attr.toStdString();
    m_tableController->insertColumn(colName, colToAdd);
}

void MainWindow::attributesViewAttributeDeleted(int index) {
    const int colToDelete = m_tableController->numBaseColumns() + (1) + index;
    m_tableController->removeColumn(colToDelete);  // +1 due to the layers column
}
