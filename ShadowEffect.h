#ifndef SHADOWEFFECT_H
#define SHADOWEFFECT_H

#include <QGraphicsEffect>
#include <QObject>
#include <QPainter>

/**https://stackoverflow.com/questions/23718827/qt-shadow-around-window*/

class ShadowEffect : public QGraphicsEffect {
  Q_OBJECT

  qreal  m_distance;
  qreal  m_blurRadius;
  QColor m_color;

  public:
  ShadowEffect(QObject* parent = nullptr);

  void   draw(QPainter* painter) override;
  QRectF boundingRectFor(const QRectF& rect) const override;

  qreal  distance() const;
  void   setDistance(const qreal& distance);
  QColor color() const;
  void   setColor(const QColor& color);
  qreal  blurRadius() const;
  void   setBlurRadius(const qreal& blurRadius);
};

#endif // SHADOWEFFECT_H
