#include "FilterView.h"
#include <QBoxLayout>
#include <QDebug>
#include <QLabel>

#include <EagleLib/part.h>

FilterView::FilterView(QWidget *parent) : QWidget(parent) {
    QVBoxLayout *vbl = new QVBoxLayout(this);
    vbl->setContentsMargins(2, 2, 2, 5);

    QLabel *lbl = new QLabel("Select Part Types to show", this);
    vbl->addWidget(lbl);

    m_listView = new QListView(this);
    m_model    = new QStandardItemModel(this);
    m_listView->setModel(m_model);
    m_listView->setSelectionMode(QAbstractItemView::SelectionMode::MultiSelection);
    connect(m_listView->selectionModel(), &QItemSelectionModel::selectionChanged, this, &FilterView::selectionChanged);

    for (int i = 0; i < eagle::Part::NumTypes; i++) {
        const eagle::Part::Type type          = static_cast<eagle::Part::Type>(1 << i);
        auto                    typeAttribute = eagle::Part::TypeAttributes.at(type);
        QStandardItem          *item          = new QStandardItem(QString::fromStdString(typeAttribute.name));
        item->setEditable(false);
        item->setToolTip(QString::fromStdString(typeAttribute.description));
        m_model->appendRow(item);
    }
    // Parts to show
    QStandardItem *item = new QStandardItem("DNP");
    item->setEditable(false);
    item->setToolTip("Parts that should not be placed (value contains \"dnp\")");
    m_model->appendRow(item);

    vbl->addWidget(m_listView);

    QHBoxLayout *hbl = new QHBoxLayout();
    hbl->setContentsMargins(0, 0, 0, 0);
    QPushButton *m_selectAllButton = new QPushButton("All", this);
    connect(m_selectAllButton, &QPushButton::released, this, &FilterView::selectAll);
    hbl->addWidget(m_selectAllButton);

    QPushButton *m_selectNoneButton = new QPushButton("None", this);
    connect(m_selectNoneButton, &QPushButton::released, this, &FilterView::clearSelection);
    hbl->addWidget(m_selectNoneButton);
    vbl->addLayout(hbl);

    m_boardLayersCheckbox = new QCheckBox("Show Part Layers");
    vbl->addWidget(m_boardLayersCheckbox);
    connect(m_boardLayersCheckbox, &QCheckBox::toggled, this, &FilterView::showBoardLayersToggled);

    QHBoxLayout *searchLayout = new QHBoxLayout();
    searchLayout->setContentsMargins(0, 0, 3, 0);
    searchLayout->setSpacing(1);

    m_searchLineEdit = new QLineEdit(this);
    m_searchLineEdit->setPlaceholderText("Search RegEx");
    searchLayout->addWidget(m_searchLineEdit);
    connect(m_searchLineEdit, &QLineEdit::textChanged, this, &FilterView::searchTextChanged);

    QPushButton *clearSearchBtn = new QPushButton(this);
    clearSearchBtn->setFixedSize(12, 12);
    auto closeIcon = this->style()->standardIcon(QStyle::StandardPixmap::SP_TitleBarCloseButton);
    clearSearchBtn->setIcon(closeIcon);
    clearSearchBtn->setIconSize(clearSearchBtn->size());
    clearSearchBtn->setFlat(true);
    searchLayout->addWidget(clearSearchBtn);
    connect(clearSearchBtn, &QPushButton::released, m_searchLineEdit, &QLineEdit::clear);

    vbl->addLayout(searchLayout);

    m_listView->selectAll();
}

void FilterView::selectAll() { m_listView->selectAll(); }
void FilterView::clearSelection() { m_listView->clearSelection(); }

void FilterView::setSelection(const std::vector<int> &indices) {
    QItemSelection selection(m_model->index(0, 0), m_model->index(m_model->rowCount(), 0));  // none selected

    for (auto i : indices) {
        const QModelIndex index = m_model->index(i, 0);
        selection.select(index, index);
    }
    m_listView->selectionModel()->select(selection, QItemSelectionModel::SelectionFlag::ClearAndSelect);
}

void FilterView::setBoardLayers(bool checked) {
    m_boardLayersCheckbox->setChecked(checked);
    m_boardLayersCheckbox->toggled(checked);
}

bool FilterView::showBoardLayers() const { return m_boardLayersCheckbox->isChecked(); }

void FilterView::select(const std::vector<int> &indices, QItemSelectionModel::SelectionFlags command) {
    for (auto i : indices) {
        auto index = m_model->index(i, 0);
        m_listView->selectionModel()->select(index, command);
    }
}

void FilterView::selectedFilterTypes(std::vector<int> &outVector) const {
    for (auto selectedIndex : m_listView->selectionModel()->selectedRows()) {
        outVector.push_back(selectedIndex.row());
    }
}

QString FilterView::searchText() { return m_searchLineEdit->text(); }