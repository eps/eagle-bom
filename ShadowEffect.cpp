#include "ShadowEffect.h"
#include <QDebug>
#include <QWidget>

ShadowEffect::ShadowEffect(QObject *parent) : QGraphicsEffect(parent), m_distance(6), m_blurRadius(20), m_color(0, 0, 0, 80) {}

qreal ShadowEffect::distance() const { return m_distance; }

void ShadowEffect::setDistance(const qreal &distance) { m_distance = distance; }

QColor ShadowEffect::color() const { return m_color; }

void ShadowEffect::setColor(const QColor &color) { m_color = color; }

qreal ShadowEffect::blurRadius() const { return m_blurRadius; }

void ShadowEffect::setBlurRadius(const qreal &blurRadius) { m_blurRadius = blurRadius; }

QRectF ShadowEffect::boundingRectFor(const QRectF &rect) const {
    qreal delta = m_blurRadius + m_distance;
    return rect.united(rect.adjusted(-delta, -delta, delta, delta));
}

QT_BEGIN_NAMESPACE
extern Q_WIDGETS_EXPORT void qt_blurImage(QPainter *p, QImage &blurImage, qreal radius, bool quality, bool alphaOnly, int transposed = 0);
QT_END_NAMESPACE

void ShadowEffect::draw(QPainter *painter) {
    if((blurRadius() + distance()) <= 0) {
        drawSource(painter);
        return;
    }

    PixmapPadMode mode = QGraphicsEffect::PadToEffectiveBoundingRect;
    QPoint        offset;
    const QPixmap px = sourcePixmap(Qt::DeviceCoordinates, &offset, mode);

    if(px.isNull()) return;

    QTransform restoreTransform = painter->worldTransform();
    painter->setWorldTransform(QTransform());

    QSize szi(px.size().width() + 2 * m_distance, px.size().height() + 2 * m_distance);

    QImage  tmp(szi, QImage::Format_ARGB32_Premultiplied);
    QPixmap scaled = px.scaled(szi);
    tmp.fill(0);
    QPainter tmpPainter(&tmp);
    tmpPainter.setCompositionMode(QPainter::CompositionMode_Source);
    tmpPainter.drawPixmap(QPointF(-distance(), -distance()), scaled);
    tmpPainter.end();

    // blur the alpha channel
    QImage blurred(tmp.size(), QImage::Format_ARGB32_Premultiplied);
    blurred.fill(0);
    QPainter blurPainter(&blurred);
    qt_blurImage(&blurPainter, tmp, blurRadius(), false, true);
    blurPainter.end();

    tmp = blurred;

    // blacken the image...
    tmpPainter.begin(&tmp);
    tmpPainter.setCompositionMode(QPainter::CompositionMode_SourceIn);
    tmpPainter.fillRect(tmp.rect(), color());
    tmpPainter.end();

    // draw the blurred shadow...
    painter->drawImage(offset, tmp);

    // draw the actual pixmap...
    painter->drawPixmap(offset, px, QRectF());

    // restore world transform
    painter->setWorldTransform(restoreTransform);
}
