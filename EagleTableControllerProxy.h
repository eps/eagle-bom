#pragma once

#include <QDebug>
#include <QRegularExpression>
#include <QSortFilterProxyModel>
#include "EagleLib/part.h"

// https://www.walletfox.com/course/qsortfilterproxymodelexample.php
// https://doc.qt.io/qt-5/qtwidgets-itemviews-customsortfiltermodel-example.html

class EagleTableControllerProxy : public QSortFilterProxyModel {
public:
    explicit EagleTableControllerProxy(QObject *parent = nullptr);

    uint32_t typeFilter() const { return m_typeFilter; }
    void     setTypeFilter(uint32_t newFilter) {
        if (newFilter != m_typeFilter) {
            m_typeFilter = newFilter;
            invalidateFilter();
        }
    }
    void setSearchRegex(const QString &str, const QList<int> &searchColumnIndices) {
        if (!str.isEmpty()) {
            // if (str != m_searchRegex.pattern()) {
            QRegularExpression re(str);
            if (re.isValid()) {
                m_searching           = true;
                m_searchColumnIndices = searchColumnIndices;
                m_searchRegex.setPattern(str);
                m_searchRegex.setPatternOptions(m_searchRegex.patternOptions() | QRegularExpression::PatternOption::CaseInsensitiveOption);
                invalidateFilter();
            } else {
                m_searching = false;
            }
            // }  // no change -> do nothing
        } else {
            m_searching = false;
            m_searchRegex.setPattern(str);
            m_searchRegex.setPatternOptions(m_searchRegex.patternOptions() | QRegularExpression::PatternOption::CaseInsensitiveOption);
            invalidateFilter();
        }
    }

public slots:

    // QSortFilterNameProxyModel interface
protected:
    bool lessThan(const QModelIndex &source_left, const QModelIndex &source_right) const;
    bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const;

    // QAbstractItemModel interface
public:
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;

private:
    uint32_t           m_typeFilter;
    QRegularExpression m_searchRegex;
    QList<int>         m_searchColumnIndices;
    bool               m_searching;
};
