#include "AttributesView.h"
#include <QBoxLayout>
#include <QDebug>
#include <QEvent>
#include <QKeyEvent>
#include <QLabel>

#include <EagleLib/part.h>

AttributesView::AttributesView(QWidget *parent) : QWidget(parent) {
    QVBoxLayout *vbl = new QVBoxLayout(this);
    vbl->setContentsMargins(2, 2, 2, 5);

    m_listView = new QListView(this);
    m_listView->installEventFilter(this);
    m_model = new QStandardItemModel(this);
    m_listView->setModel(m_model);
    m_listView->setSelectionMode(QAbstractItemView::SelectionMode::MultiSelection);
    m_listView->selectAll();
    connect(m_listView->selectionModel(), &QItemSelectionModel::selectionChanged, this,
            &AttributesView::selectionChanged);

    m_addAttributeLineEdit = new QLineEdit(this);
    m_addAttributeLineEdit->setPlaceholderText("Add attribute");
    connect(m_addAttributeLineEdit, &QLineEdit::returnPressed, this, &AttributesView::lineEditReturnPressed);

    vbl->addWidget(m_listView);
    vbl->addWidget(m_addAttributeLineEdit);
}

void AttributesView::selectAll() { m_listView->selectAll(); }
void AttributesView::clearSelection() { m_listView->clearSelection(); }

void AttributesView::setSelection(const std::vector<int> &indices) {
    QItemSelection selection(m_model->index(0, 0), m_model->index(m_model->rowCount(), 0));  // none selected

    for (auto i : indices) {
        const QModelIndex index = m_model->index(i, 0);
        selection.select(index, index);
    }
    m_listView->selectionModel()->select(selection, QItemSelectionModel::SelectionFlag::ClearAndSelect);
}

void AttributesView::select(const std::vector<int> &indices, QItemSelectionModel::SelectionFlags command) {
    for (auto i : indices) {
        auto index = m_model->index(i, 0);
        m_listView->selectionModel()->select(index, command);
    }
}

void AttributesView::select(int index, QItemSelectionModel::SelectionFlags command) {
    auto idx = m_model->index(index, 0);
    if (idx.isValid()) m_listView->selectionModel()->select(idx, command);
}

void AttributesView::selectedAttributes(std::list<std::string> &outAttributes) {
    auto selectedRows = m_listView->selectionModel()->selectedRows();
    for (auto selectedIndex : selectedRows) {
        auto item = m_model->itemFromIndex(selectedIndex);
        outAttributes.push_back(item->text().toStdString());
    }
}

int AttributesView::addAttribute(const QString &attr) {
    auto newItem = new QStandardItem(attr);
    newItem->setEditable(false);
    int insertionIndex = 0;
    if (m_model->rowCount() > 0) {
        for (; insertionIndex < m_model->rowCount(); insertionIndex++) {
            auto item = m_model->item(insertionIndex);
            if (attr < item->text()) {
                m_model->insertRow(insertionIndex, newItem);
                break;
            }
        }
        if (insertionIndex == m_model->rowCount()) { m_model->appendRow(newItem); }
    } else {
        m_model->appendRow(newItem);
    }
    return insertionIndex;
}
int AttributesView::addAttribute(const std::string &attr) { return addAttribute(QString::fromStdString(attr)); }

void AttributesView::deleteAttribute(int index) {
//    QModelIndex idx = m_model->index(index, 0);
    m_model->removeRow(index);
    
}

void AttributesView::setAttributes(const std::vector<std::string> &attributes) {
    m_model->clear();
    for (const std::string &attribute : attributes) { addAttribute(attribute); }
}
bool AttributesView::attributeExists(const QString &attribute) {
    auto items = m_model->findItems(attribute);
    return !items.isEmpty();
}

void AttributesView::lineEditReturnPressed() {
    QString        text = m_addAttributeLineEdit->text().toUpper();

    if (!attributeExists(text)) {
        int  insertionIndex = addAttribute(text);
        emit attributeAdded(text, insertionIndex);
        // call select after the data has been propagated into the parts tableview
        // this will make the mainwindow to show/unhide the newly created column
        select(insertionIndex, QItemSelectionModel::SelectionFlag::Select);
    }

    m_addAttributeLineEdit->selectAll();
}

bool AttributesView::eventFilter(QObject *obj, QEvent *event) {
    if (event->type() == QEvent::KeyRelease) {
        if (QKeyEvent *ke = dynamic_cast<QKeyEvent *>(event)) {
            if (ke->key() == Qt::Key_Delete) {
                auto indexToDelete = m_listView->selectionModel()->currentIndex();
                emit attributeDeleted(indexToDelete.row());
                m_listView->selectionModel()->blockSignals(true);
                deleteAttribute(indexToDelete.row() );
                m_listView->selectionModel()->blockSignals(false);
            }
        }
    }
    return QObject::eventFilter(obj, event);
}
