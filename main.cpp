#include "MainWindow.h"
#include "settings.h"

#include <QApplication>
#include <QGuiApplication>
#include <QScreen>

int main(int argc, char *argv[]) {
	QApplication a(argc, argv);
    Settings::shared().load();
    MainWindow w;
    QRect	   displayRect = QGuiApplication::primaryScreen()->geometry();
	w.show();
	w.resize(displayRect.width() * 2 / 3, displayRect.height() * 2 / 3);
	return a.exec();
}
