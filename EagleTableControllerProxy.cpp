#include "EagleTableControllerProxy.h"
#include "EagleTableController.h"

EagleTableControllerProxy::EagleTableControllerProxy(QObject *parent)
    : QSortFilterProxyModel(parent)
    , m_typeFilter((1 << 15) - 1)
    , m_searchRegex()
    , m_searchColumnIndices()
    , m_searching(false) {}

bool EagleTableControllerProxy::lessThan(const QModelIndex &source_left, const QModelIndex &source_right) const {
    if (source_left.isValid() && source_right.isValid()) {
        const EagleTableController *controller = static_cast<const EagleTableController *>(sourceModel());
        int                         col        = source_left.column();
        const eagle::Part          *lPart      = controller->partAt(source_left.row());
        const eagle::Part          *rPart      = controller->partAt(source_right.row());
        assert(lPart && rPart);
        if (controller->grouping()) {
            switch (col) {
                case 0: /*Qty*/
                    return sourceModel()->data(source_left).toInt() < sourceModel()->data(source_right).toInt();
                case 1: /*Value*/ return lPart->lessThan(*rPart, eagle::Part::Sort::ByValue);
                case 2: /*Parts*/ return lPart->lessThan(*rPart, eagle::Part::Sort::ByName);
                default: {
                    QString lString = sourceModel()->data(source_left).toString();
                    QString rString = sourceModel()->data(source_right).toString();
                    return lString < rString;
                }
            }
        } else {
            if (const eagle::Part *lPart = controller->partAt(source_left.row())) {
                if (const eagle::Part *rPart = controller->partAt(source_right.row())) {
                    switch (col) {
                        case 0: /*part name*/ return lPart->lessThan(*rPart, eagle::Part::Sort::ByName);
                        case 1: /*value*/ return lPart->lessThan(*rPart, eagle::Part::Sort::ByValue);
                        default: {
                            QString lString = sourceModel()->data(source_left).toString();
                            QString rString = sourceModel()->data(source_right).toString();
                            return lString < rString;
                        }
                    }
                }
            }
        }
    }
    return false;
}

bool EagleTableControllerProxy::filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const {
    EagleTableController *controller = static_cast<EagleTableController *>(sourceModel());
    if (const eagle::Part *part = controller->partAt(sourceRow)) {
        bool showDNPParts              = m_typeFilter & (1 << 14);                                // A, lsb
        bool partContainsDNP_not       = !QString::fromStdString(part->value()).contains("dnp");  // B
        bool showPartsWithoutFootprint = m_typeFilter & (1 << 13);                                // C
        bool partHasFootprint          = part->hasFootprint();                                    // D
        bool showPartType              = m_typeFilter & part->type();                             // E, msb
        bool regexFound                = false;                                                   // F, G = m_searching

        if (m_searching) {
            for (auto columnIndex : m_searchColumnIndices) {
                auto cellString = sourceModel()->data(sourceModel()->index(sourceRow, columnIndex)).toString();
                QRegularExpressionMatch match =
                    m_searchRegex.match(cellString, 0, QRegularExpression::MatchType::PartialPreferFirstMatch);
                regexFound = match.hasMatch();
                if (regexFound) { break; }
            }
        }

        return ((showPartType) &&
                ((showDNPParts || partContainsDNP_not) && (showPartsWithoutFootprint || partHasFootprint))) &&
               (regexFound || !m_searching);
    }
    assert(true);  // part could not be found
    return false;
}

QVariant EagleTableControllerProxy::headerData(int section, Qt::Orientation orientation, int role) const {
    return sourceModel()->headerData(section, orientation, role);
}