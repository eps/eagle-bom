#ifndef SETTINGS_H
#define SETTINGS_H

#include <QDebug>
#include <list>
#include <memory>
#include <nlohmann/json.hpp>
#include <string>
#include <vector>

class Settings {
public:
    typedef struct settings_data {
    private:
        unsigned int     m_numRecentFiles;
        bool             m_viewGroupedParts;
        bool             m_viewBoardLocations;
        std::vector<int> m_selectedFilterTypes;

    public:
        std::string            settingsPath;
        std::list<std::string> recentFiles;
        std::list<std::string> visibleAttributes;

    public:
        settings_data(unsigned int numRecentFiles = 5, bool viewGroupedParts = false, bool viewBoardLocations = false);
        void         setNumRecentFiles(unsigned int value);
        unsigned int numRecentFiles() const { return m_numRecentFiles; }
        void         setViewGroupedParts(bool newValue) { m_viewGroupedParts = newValue; }
        bool         viewGroupedParts() const { return m_viewGroupedParts; }
        void         setViewBoardLocations(bool newValue) { m_viewBoardLocations = newValue; }
        bool         viewBoardLocations() const { return m_viewBoardLocations; }
        void setSelectedFilterTypes(const std::vector<int> &selectedTypes) { m_selectedFilterTypes = selectedTypes; }
        const std::vector<int> &selectedFilterTypes() const { return m_selectedFilterTypes; }

    } settings_data_t;

    static settings_data_t defaultSettings;

private:
    settings_data_t m_settings;

    Settings();

public:
    //	static std::vector<std::string> connectionTypesAsVectorOfStr();

    static Settings &shared();
    Settings(const Settings &s)           = delete;
    Settings &operator=(const Settings &) = delete;

    void applyDefaultSettings(bool preserveRecentFiles = true);
    void copy(Settings::settings_data_t &settings) const;
    void updateSettings(const Settings::settings_data_t &newSettings);
    void load();
    void save();

    unsigned int                  getNumRecentFiles() const;
    void                          setNumRecentFiles(unsigned int value);
    std::list<std::string>        getRecentFiles() const;
    void                          setRecentFiles(const std::list<std::string> &value);
    void                          recentFilesPush(const std::string &file);

    /**
     * @brief Removes the given file from the list
     * 
     * @param file Path to be removed
     * @return true if one or more files with the given name were removed
     * @return false file not found in list
     */
    bool                          recentFilesRemove(const std::string &file);
    const std::string            *recentFilesPeak() const;
    std::string                   getSettingsPath() const;
    void                          setSettingsPath(const std::string &value);
    const std::list<std::string> &getVisibleAttributes() const;
    void                          setVisibleAttributes(const std::list<std::string> &attributes);

    void setViewGroupedParts(bool newValue) { m_settings.setViewGroupedParts(newValue); }
    bool viewGroupedParts() const { return m_settings.viewGroupedParts(); }
    void setViewBoardLocations(bool newValue) { m_settings.setViewBoardLocations(newValue); }
    bool viewBoardLocations() const { return m_settings.viewBoardLocations(); }
    void setSelectedFilterTypes(const std::vector<int> &selectedTypes) {
        m_settings.setSelectedFilterTypes(selectedTypes);
    }
    const std::vector<int> &selectedFilterTypes() const { return m_settings.selectedFilterTypes(); }
};

extern void to_json(nlohmann::json &j, const Settings::settings_data_t &s);
extern void from_json(const nlohmann::json &j, Settings::settings_data_t &s);

extern QDebug operator<<(QDebug debug, const std::string &str);
#endif  // SETTINGS_H
